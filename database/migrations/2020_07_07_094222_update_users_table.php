<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users')) {
            Schema::table('users', function (Blueprint $table) {
                $table->string('subdomain');
                $table->string('mobile');
                $table->string('subdb');
                $table->tinyInteger('mobile_verified')->default(0);
                $table->tinyInteger('mobile_otp')->length(10)->nullable();
                $table->unsignedBigInteger('referrer_id')->nullable();
                $table->foreign('referrer_id')->references('id')->on('users');
                $table->string('referral_token');
                $table->string('referral_code');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('maindb')->table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('referrer_id')->nullable();
            $table->foreign('referrer_id')->references('id')->on('users');

        });
    }
}
