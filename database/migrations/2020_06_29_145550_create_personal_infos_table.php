<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('personal_infos')) {
            Schema::create('personal_infos', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('profession');
                $table->string('email');
                $table->string('mobile');
                $table->string('whatsapp');
                $table->string('website');
                $table->longtext('description');
                $table->string('skype_id');
                $table->string('fb_msg_url');
                $table->string('profile_img');
                $table->string('company_logo');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_infos');
    }
}
