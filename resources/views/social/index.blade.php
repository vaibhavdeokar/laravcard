@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Social Link List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('social.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Social Name</th>
            <th>Social Link</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($socials as $social)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $social->sname }}</td>
            <td>{{ $social->svalue }}</td>
            <td>
                <form action="{{ route('social.destroy',$social->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('social.show',$social->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('social.edit',$social->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $socials->links() !!}
      
@endsection