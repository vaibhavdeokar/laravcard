@extends('layouts.app')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add Social Link</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('social.index') }}"> Back</a>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('social.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{-- <input type="text" name="sname" class="form-control" placeholder="Name"> --}}
                <select class="form-control" name="sname">
                    <option value="" selected="selected">Choose Social Link</option>
                    <option value="fa-facebook-f">Facebook</option>
                    <option value="fa-twitter">Twitter</option>
                    <option value="fa-google">Google</option>
                    <option value="fa-linkedin-in">LinkedIn</option>
                    <option value="fa-youtube">YouTube</option>
                    <option value="fa-instagram">Instagram</option>
                    <option value="fa-quora ">Quora</option>
                    <option value="fa-google-play">Google Play Store</option>
                    <option value="fa-app-store-ios">App Store IOS</option>
                    <option value="fa-bullhorn">Podcast</option>
                    <option value="fa-blogger">Blogger</option>
                </select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Detail:</strong>
                <input type="url" name="svalue" class="form-control" placeholder="e.g. https://paypal.me/yourname">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
   
</form>
@endsection