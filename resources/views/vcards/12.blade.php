<!DOCTYPE html>
<html>

<head>
    <title>Sandip Pawar's Digital Business Card</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> -->
    <meta property="og:image" content="https://card.trinitysolutionz.in/assets/images/profile2.jpg">
    <meta property="og:title" content="Roosevelt Mathew's Digital Business Card" />
    <meta property="og:description" content="Co-Founder, Trinity Solutionz" />
    <!-- bellow the link of font awesome5 -->
    <script src="https://kit.fontawesome.com/9b1326529b.js"></script>

    <link href= "{{$vcardPath}}/assets/style.css " rel="stylesheet" type="text/css" media="all" />
</head>

<body>
    <div class="vCardWrapper">
        <div class="vcard_carousel_Sec">
            <div id="vcard_carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#vcard_carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#vcard_carousel" data-slide-to="1"></li>
                    <li data-target="#vcard_carousel" data-slide-to="2"></li>
                    <li data-target="#vcard_carousel" data-slide-to="3"></li>
                    <li data-target="#vcard_carousel" data-slide-to="4"></li>
                </ul>
                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src= "{{$vcardPath}}/assets/images/sliderlogo.jpg " alt="">
                    </div>
                    <div class="carousel-item ">
                        <img src= "{{$vcardPath}}/assets/images/slider1.jpg " alt="">
                    </div>
                    <div class="carousel-item">
                        <img src= "{{$vcardPath}}/assets/images/slider2.jpg " alt="">
                    </div>
                    <div class="carousel-item">
                        <img src= "{{$vcardPath}}/assets/images/slider3.jpg " alt="">
                    </div>
                    <div class="carousel-item">
                        <img src= "{{$vcardPath}}/assets/images/slider4.jpg " alt="">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#vcard_carousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#vcard_carousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>
        </div>
        <!-- End of Vcard Carousel section  -->

        <!-- Start bottom header section -->

        <div class="profile-header">
            <div class="profile_img">
                <img src= "{{asset('storage/userdata/'.$data->profile_logo)}}" alt="profile" style="cursor:pointer">
            </div>
            <span>
                <h4>Sandip Pawar</h4>
                <address>
                    <i> Operations</i>
                </address>
            </span>
        </div>
        <!-- End of bottom header section -->



        <div class="sepratorbox mt-30">
            <span id="sepr-left" class="sepr"></span>
            <span id="sepr-center" class="sepr"></span>
            <span id="sepr-right" class="sepr"></span>
        </div>

        <div class="clearfix"></div>


        <div class="icons">
            <ul>
                <li>
                    <div class="icon-single" id="hs-1">
                        <a href="https://wa.me/919822627278" target="_blank"><i class="fab fa-whatsapp fa-lg"></i></a>
                    </div>
                </li>
                <li>
                    <div class="icon-single" id="hs-2">
                        <a href="tel:+919822627278"><i class="fas fa-phone-alt"></i></a>
                    </div>
                </li>
                <li>
                    <div class="icon-single" id="hs-3">
                        <a href="#" target="_blank"><i class="fas fa-map-marker-alt"></i></a>
                    </div>
                </li>
                <li>
                    <div class="icon-single" id="hs-4">
                        <a href="#/" target="_blank"><i class="far fa-address-card"></i>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="icon-single" id="hs-5">
                        <a href="sms:+919822627278?body=Hello" target="_blank"><i class="far fa-comment-dots"></i></a>
                    </div>
                </li>
                <li>
                    <div class="icon-single" id="hs-6">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#ShareModal" data-original-title="Copy to Clipboard">
                            <i class="fas fa-share"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="social_media">
            <ul>
                <li>
                    <a href="https://www.facebook.com/" target="_blank">
                        <i class="fab fa-facebook-square fa-lg" aria-hidden="true" style="
                        color: #3b5998;"></i>
                    </a>
                </li>
                <li>
                    <a href="https://instagram.com/" target="_blank" style="
                    background: radial-gradient(circle at 30% 107%, #fdf497 0%, #fdf497 5%, #fd5949 45%, #d6249f 60%, #285AEB 90%);
                    -webkit-background-clip: text;
                    background-clip: text;
                    -webkit-text-fill-color: transparent;
                    border-radius: 7px;
                    padding: 2px;
                ">
                        <i class="fab fa-instagram-square fa-lg" aria-hidden="true"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.youtube.com/" target="_blank">

                        <i class="fab fa-youtube-square fa-lg" aria-hidden="true" style="
                        color: #f00;"></i>
                    </a>
                </li>
                <li>
                    <a href="https://linkedin.com/" target="_blank">
                        <i class="fab fa-linkedin fa-lg" aria-hidden="true" style="color: #0e76a8;"></i>
                    </a>
                </li>



                <li>
                    <a href="https://twitter.com/" target="_blank">

                        <i class="fab fa-twitter-square fa-lg" aria-hidden="true" style="color: #00acee;"></i>
                    </a>
                </li>
            </ul>
            <hr>

        </div>

        <div id="main-icon">
            <!-- <div style="width: 27%;">
                <a href="tel:+919822627278">
                    <span>
                        <h5><i class="fas fa-mobile-alt"></i> Mobile</h5>
                        <span> +91 93205 05567</span>
                    </span>
                </a>
            </div> -->
            <div style="width: 68%;">
                <a href="mailto:sandip@theintegratedsolutions.com">
                    <span>
                        <h5><i class="far fa-envelope"></i> Email</h5>
                        <span>sandip@theintegratedsolutions.com</span>
                    </span>
                </a>
            </div>
            <div>
                <a href="http://theintegratedsolutions.com/" target="_blank">
                    <span>
                        <h5><i class="fas fa-globe"></i> Website</h5>
                        <span>
                            theintegratedsolutions.com
                        </span>
                    </span>
                </a>
            </div>
        </div>

        <!-- paymenet and google review section  -->
        <div class="btnsec pt-20 pb-20">
            <!-- <a href="#/" class="btn1">Make Payment <i class="fas fa-rupee-sign"></i></a> -->
            <a href="#/" class="btn1">Google Review <i class="fab fa-google"></i></a>
        </div>
        <!-- End of and google review section  -->



        <div class="sepratorbox">
            <span id="sepr-left" class="sepr"></span>
            <span id="sepr-center" class="sepr"></span>
            <span id="sepr-right" class="sepr"></span>
        </div>

        <!-- Start  content information section  -->
        <div class="Soltionsec pt-30 pb-30 ">
            <header class="text-center mb-30">
                <h3><span class="fas fa-shield-alt"></span> What We Offer</h3>
            </header>
            <main>
                <div class="clearfix">
                    <ul class="services_list">
                        <li>Warehousing & Contract Logistics</li>
                        <li>Freight Forwarding & Custom Clearnce</li>


                    </ul>
                    <ul class="services_list">
                        <li>Packaging Solutions</li>
                        <li>Trade Facilitation & Liaison</li>
                    </ul>
                </div>
            </main>
        </div>



        <div class="clearfix"></div>

        <div class="sepratorbox">
            <span id="sepr-left" class="sepr"></span>
            <span id="sepr-center" class="sepr"></span>
            <span id="sepr-right" class="sepr"></span>
        </div>

        <div class="clearfix"></div>

        <!-- paymenet and google review section  -->
        <div class="btnsec pt-20 pb-20">
            <a href="#/" class="btn1" data-toggle="modal" data-target="#aboutUs">About Us <span class="fas fa-users fa-lg"></span></a>
            <a href="#/" class="btn1" data-toggle="modal" data-target="#media">Media <span class="fas fa-images fa-lg"></span></a>
        </div>
        <!-- End of and google review section  -->


        <!--
        <div class="contentinfo">
            <div id="accordion">
                <div class="question">
                    <header>
                        <h3><span class="fas fa-map-marker-alt"></span> Address</h3>
                        <i class="fas fa-chevron-down"></i>
                    </header>
                    <main>


                    </main>
                </div>

                <div class="question">
                    <header>
                        <h3><span class="fas fa-shield-alt"></span> Solutions</h3>
                        <i class="fas fa-chevron-down"></i>
                    </header>
                    <main>
                        <div class="clearfix">
                            <ul class="services_list">
                                <li></li>


                            </ul>
                            <ul class="services_list">
                                <li></li>


                            </ul>
                        </div>
                    </main>
                </div>

                <div class="question">
                    <header>
                        <h3><span class="fas fa-users fa-lg"></span> About Us</h3>
                        <i class="fas fa-chevron-down"></i>
                    </header>
                    <main>
                        <div class="aboutbox">


                        </div>
                    </main>
                </div>

                <div class="question">
                    <header>
                        <h3><span class="fas fa-images fa-lg"></span> Gallery</h3>
                        <i class="fas fa-chevron-down"></i>
                    </header>
                    <main>

                    </main>
                </div>


            </div>
        </div>-->
        <!-- End of  content information section  -->



        <div class="clearfix"></div>

        <!-- Start social media section -->

        <!-- End of social media  section -->

        <div class="footerSec text-center">
            <p>Created With <a href="https://www.easycards.in/">EasyCards.in</a>
            </p>
        </div>

    </div>
    <!-- End of vCardWrapper -->





    <!-- start popup modal section -->

    <!-- The Modal -->
    <div class="modal" id="aboutUs">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">About Us</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="aboutbox">
                        <h5>OVERVIEW</h5>

                        <p>At Integrated Solutions ( innovation and expertise is the key philosophy that drives our growth Established in 2010 born to provide more sophisticated but simpler and cost effective solutions to the EXIM TRADE</p>

                        <p>With its office in Mumbai helm of international business in India Integrated aims at providing result driven both 3 PL 4 PL solutions to companies engaged in International Trade With the 100 financial growth last year we Targeted
                            to cross 25 Cr by FY 2022</p>


                        <p>A combination of experience and innovation gives us the edge to deliver more, every time</p>

                        <h5> VISION</h5>
                        <p>To be an Indian multinational providing real end supply chain solutions across continents</p>

                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- End popup modal section -->

    <!-- The Modal -->
    <div class="modal" id="media">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Media</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="mediabox text-center">
                        <p>To know more about our company details download our brochurea.</p>
                        <div class="bro-down">
                            <a href= "{{$vcardPath}}/assets/images/brochures/FTWZ.pdf " download><img src= "{{$vcardPath}}/assets/images/download_brochure.png " alt=""></a>

                            <h4>Free Trade Warehousing Zone</h4>
                        </div>
                        <div class="bro-down">
                            <a href= "{{$vcardPath}}/assets/images/brochures/IOR_Services.pdf " download><img src= "{{$vcardPath}}/assets/images/download_brochure.png " alt=""></a>
                            <h4>Importer of Record Services</h4>
                        </div>

                        <div class="bro-down">
                            <a href= "{{$vcardPath}}/assets/images/brochures/Warehousing&Distribution.pdf " download><img src= "{{$vcardPath}}/assets/images/download_brochure.png " alt=""></a>
                            <h4>Warehousing and Distribution </h4>
                        </div>

                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <!-- End popup modal section -->



    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
          Open modal
        </button> -->

    <!-- The Modal -->
    <div class="modal fade" id="ShareModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title">SHARE VCARD</h4>
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="shareModalinner text-center">
                        <h4>Share my VCard in your network</h4>
                        <div class="sharbox  pt-30 pb-20">
                            <div>
                                <a href="https://api.whatsapp.com/send?&text=Please%20find%20Digital%20Business%20Card%20of%20Sandip%20Pawar%20for%20your%20reference.%20%20http://card.theintegratedsolutions.com/">
                                    <img src= "{{$vcardPath}}/assets/images/WhatApp.png " class="img-fluid" alt="">
                                </a>
                            </div>
                            <div>
                                <a href="sms:?body=Please%20find%20Digital%20Business%20Card%20of%20Sandip%20Pawar%20for%20your%20reference.%20%20http://card.theintegratedsolutions.com/">
                                    <img src= "{{$vcardPath}}/assets/images/SMS.png " class="img-fluid" alt="">
                                </a>
                            </div>
                            <div>
                                <a href="mailto:?subject=Digital%20Business%20Card%20of%20Sandip%20Pawar&body=Hi%20Friend,%3C/br%3E%3C/br%3EPlease%20view%20my%20Digital%20Business%20Card%20http://card.theintegratedsolutions.com/%20and%20feel%20free%20to%20get%20in%20touch%20with%20me%20over%20mail%20,%20Message%20or%20phone.%3C/br%3E%3C/br%3EWill%20be%20glad%20to%20serve%20you.%3C/br%3E%3C/br%3ERegards,%3C/br%3ESandip%20Pawar">
                                    <img src= "{{$vcardPath}}/assets/images/email.png " class="img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>














    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#accordion header').click(function() {
                $(this).next()
                    .slideToggle(200)
                    .closest('.question')
                    .toggleClass('active')
                    .siblings()
                    .removeClass('active')
                    .find('main')
                    .slideUp(200);
            })
        });
    </script>
</body>

</html>