<!doctype html>
<html>
    <head>
        <title>Laravel Notify</title>
        @notifyCss
    </head>
    <body>
        
        
        @include('notify::messages')

        @notifyJs
        @yield('content')
    </body>
</html>