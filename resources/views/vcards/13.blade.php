<!DOCTYPE html>
<html>

<head>
    <title>Vcard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> -->

    <!-- bellow the link of font awesome5 -->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    <link href="{{$vcardPath}}/assets/style.css" rel="stylesheet" type="text/css" media="all" />
</head>

<body>

    <div class="vjCover">

    </div>

    <div class="vjCoverwhite">
        <img src="{{asset('storage/userdata/'.$data->profile_logo)}}" class="vjUserPhoto">
        <h1>{{$data->first_name}} {{$data->last_name}}</h1>
        <p class="vjProfession">{{$data->profession}}</p>
        <div class="vjJagya20"></div>
    </div>

    <div class="vjAkhuContainer">
        <div class="vjJagya5"></div>
        <!-- CONTACT ME CTA -->
        <div>
            <p class="vjCTAheading">Contact :</p>

            <div class="vjCTAinfobox">
                <a href="tel:+91123456789">
                    <img src="{{$vcardPath}}/assets/images/call.png" class="vjCTAimage">
                    <span class="vjctamaintext">Call Me</span>
                </a>
            </div>
            <div class="vjCTAinfobox">
                <a href="#/">
                    <img src="{{$vcardPath}}/assets/images/email.png" class="vjCTAimage">
                    <span class="vjctamaintext">Email Me</span>
                </a>
            </div>

            <div class="vjCTAinfobox">
                <a href="#/">
                    <img src="{{$vcardPath}}/assets/images/whatsapp.png" class="vjCTAimage">
                    <span class="vjctamaintext">Whatsapp</span>
                </a>
            </div>

            <div class="vjCTAinfobox md-trigger" href="#myModal1" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/save.png" class="vjCTAimage">
                <span class="vjctamaintext">Save Me</span>
            </div>
            <div class="vjCTAinfobox md-trigger" href="#myModal2" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/pay.png" class="vjCTAimage">
                <span class="vjctamaintext">Pay Me</span>
            </div>
        </div>




        <div>
            <p class="vjCTAheading">Profile :</p>
            <div class="vjCTAinfobox md-trigger" href="#myModal3" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/testimonials.png" class="vjCTAimage">
                <span class="vjctamaintext">Testimony</span>
            </div>
            <div class="vjCTAinfobox md-trigger" href="#myModal4" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/education.png" class="vjCTAimage">
                <span class="vjctamaintext">Education</span>
            </div>
            <div class="vjCTAinfobox md-trigger" href="#myModal5" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/share.png" class="vjCTAimage">
                <span class="vjctamaintext">Share This</span>
            </div>
        </div>
        <!-- MY PROFILE CTA PATYU -->



        <!-- COMPANY CTA -->

        <div>
            <p class="vjCTAheading">Company :</p>
            <div class="vjCTAinfobox  md-trigger" href="#myModal6" data-toggle="modal">
                <img src="{{$vcardPath}}/assets/images/company.png" class="vjCTAimage">
                <span class="vjctamaintext">Company</span>
            </div>

            <div class="vjCTAinfobox ">
                <a href="http://thorat.tech" target="_blank">
                    <img src="{{$vcardPath}}/assets/images/website.png" class="vjCTAimage">
                    <span class="vjctamaintext">Website</span>
                </a>
            </div>
        </div>


        <div>
            <p class="vjCTAheading">Follow Me :</p>
            <a href="http://google.com" class="vjShareContainer" target="_blank">
                <i class="fab fa-instagram vjsocialLink"></i>
            </a>
        </div>


        <div class="vjJagya20"></div>

    </div>

    <div class="vjcopyrightcontainer">
        <a href="https://thorat.tech" target="_blank" class="vjNoDecoration"> 	&copy; 2020 digital marketing</a>
    </div>



    <!-- start popup modal section -->


    <!-- start popup modal section -->

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 1111111111111111111111111111
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <!-- End popup modal section -->

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 2222222222222222222
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 333333333333333
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 44444444444
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 5555555555555
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 6666666666
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 7777777777777
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 8888888888888
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 999999999
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal10" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 101010101010
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal11" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 11,11,11,11,11
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <!-- End popup modal section -->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</body>

</html>