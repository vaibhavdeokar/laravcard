
<!DOCTYPE html>
<html>

<head>
    <title>Vcard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet"> -->

    <!-- bellow the link of font awesome5 -->
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>

    
    <link href="{{$vcardPath}}/assets/style.css" rel="stylesheet" type="text/css" media="all" />

</head>

<body>
    <div class="vjCover">

    </div>

    <div class="vjCoverwhite">

        <!-- User No Profile Pic -->

        <img src="{{asset('storage/userdata/'.$data->profile_logo)}}" class="vjUserPhoto">
        <!-- User nu First Name and Last Name -->
    <h1>{{$data->first_name}} {{$data->last_name}}</h1>

        <!-- User nu Profession -->
    <p class="vjProfession">{{$data->profession}}</p>

        <div class="vjJagya20"></div>
    </div>

    <div class="vjAkhuContainer">

        <div class="vjJagya5"></div>

        <!-- CONTACT ME CTA -->

        <div>
            <p class="vjCTAheading">Contact :</p>


            <!-- In all below HREF, there will be user data -->
            @if ($data->mobile != '')
                <div class="vjCTAinfobox"><a href="tel:{{$data->mobile}}"><i
                class="fas fa-phone fa-flip-horizontal vjCTAimage"></i><span class="vjctamaintext">Call
                Me</span></a></div>
            @endif
            @if ($data->email != '')
                <div class="vjCTAinfobox"><a href="mailto:support@thorat.tech"><i
                        class="fas fa-envelope  vjCTAimage"></i><span class="vjctamaintext">Email Me</span></a></div>
            @endif
            @if ($data->phone !='')
                <!-- In whatsapp there is country code without + symbol -->
                <div class="vjCTAinfobox"><a href="https://wa.me/91{{$data->phone}}?text={{urlencode('Hi This is for me')}}"><i class="fab fa-whatsapp vjCTAimage"></i><span
            class="vjctamaintext">Whatsapp</span></a></div>
            @endif
            
            @if ($data->skype_id != '')
                <!-- -skype-name- is the "skypeid" -->
                <div class="vjCTAinfobox"><a href="skype:{{$data->skype_id}}?chat"><i class="fab fa-skype vjCTAimage"></i><span
                        class="vjctamaintext">Skype</span></a></div>
            <!-- user will enter messenger link -->
            @endif
            @if ($data->fb_msg)
                <div class="vjCTAinfobox"><a href="https://m.me/{{$data->fb_msg}}"><i
                    class="fab fa-facebook-messenger vjCTAimage"></i><span class="vjctamaintext">FB Chat</span></a>
            </div>
            @endif
            <div class="vjCTAinfobox"><a href="/savecard"><i
                class="fas fa-user-plus vjCTAimage"></i><span class="vjctamaintext">Save Me</span></a>
        </div>
                

        {{-- <div class="vjCTAinfobox" href="/makecard" data-toggle="modal"><i class="fas fa-user-plus vjCTAimage"></i><span class="vjctamaintext">Save Me</span></div> --}}

            <div class="vjCTAinfobox" href="#myModal2" data-toggle="modal"><i class="fas fa-credit-card vjCTAimage"></i><span class="vjctamaintext">Pay Me</span></div>


        </div>
        <!-- CONTACT ME CTA PATYU -->


        <div>
            <p class="vjCTAheading">Profile :</p>

            <div class="vjCTAinfobox" href="#myModal3" data-toggle="modal"><i class="fas fa-user-tie vjCTAimage"></i><span class="vjctamaintext">About Me</span></div>
            @if (count($skills)< 0)
                <div class="vjCTAinfobox" href="#myModal4" data-toggle="modal"><i class="fas fa-star vjCTAimage"></i><span class="vjctamaintext">Skills</span></div>
            @endif
            @if (count($achievements)<0)
             <div class="vjCTAinfobox" href="#myModal5" data-toggle="modal"><i class="fas fa-trophy vjCTAimage"></i><span class="vjctamaintext">Milestones</span></div>
            @endif
            @if (count($testimonies)<0)
                <div class="vjCTAinfobox" href="#myModal6" data-toggle="modal"><i class="fas fa-thumbs-up vjCTAimage"></i><span class="vjctamaintext">Testimony</span></div>
            @endif
            @if (count($experiens)<0)
                <div class="vjCTAinfobox" href="#myModal7" data-toggle="modal"><i class="fas fa-briefcase vjCTAimage"></i><span class="vjctamaintext">Experience</span></div>
            @endif
            @if (count($educations) < 0)
                <div class="vjCTAinfobox" href="#myModal8" data-toggle="modal"><i class="fas fa-graduation-cap vjCTAimage"></i><span class="vjctamaintext">Education</span></div>  
            @endif
            <div class="vjCTAinfobox" href="#myModal9" data-toggle="modal"><i class="fas fa-share-alt vjCTAimage"></i><span class="vjctamaintext">Share This</span></div>
            <div class="vjCTAinfobox" href="#qrcodeModal" data-toggle="modal"><i class="fas fa-share-alt vjCTAimage"></i><span class="vjctamaintext">QR Code</span></div>

        </div>
        <!-- MY PROFILE CTA PATYU -->



        <!-- COMPANY CTA -->

        <div>
            <p class="vjCTAheading">Company :</p>
            <div class="vjCTAinfobox" href="#myModal10" data-toggle="modal"><i class="fas fa-building vjCTAimage"></i><span class="vjctamaintext">Company</span></div>
            <div class="vjCTAinfobox" href="#myModal11" data-toggle="modal"><i class="fas fa-handshake vjCTAimage"></i><span class="vjctamaintext">Solutions</span></div>
            <div class="vjCTAinfobox"><a href="{{$data->website}}" target="_blank"><i
                        class="fas fa-globe vjCTAimage"></i><span class="vjctamaintext">Website</span></a></div>
            <div class="vjCTAinfobox"><a href="{{$data->cmap}}" target="_blank"><i class="fas fa-map-marker-alt vjCTAimage"></i><span
                        class="vjctamaintext">Location</span></a></div>


        </div>
        <!-- COMPANY CTA PATYU -->



        <!-- SOCIAL FOLLOW -->
        <div>
            <p class="vjCTAheading">Follow Me :</p>

            <!-- ALL DYNAMIC LINK -->
            <a href="https://www.facebook.com/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-facebook-f vjsocialLink"></i></a>
            <a href="https://twitter.com/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-twitter vjsocialLink"></i></a>
            <a href="https://plus.google.com/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-google vjsocialLink"></i></a>
            <a href="https://www.instagram.com/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-instagram vjsocialLink"></i></a>
            <a href="https://www.linkedin.com/in/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-linkedin-in vjsocialLink"></i></a>
            <a href="https://www.quora.com/" class="vjShareContainer" target="_blank"><i
                    class="fab fa-quora vjsocialLink"></i></a>
        </div>
        <!-- SOCIAL FOLLOW PATYU -->

        <div class="vjJagya20"></div>

    </div>


    <div class="vjcopyrightcontainer">
        <a href="https://thorat.tech" target="_blank" class="vjNoDecoration"> &copy; 2020 digital marketing</a>
    </div>

    <!-- href="#myModal1" data-toggle="modal" -->





    <!-- start popup modal section -->

    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 1111111111111111111111111111
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <!-- End popup modal section -->

    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 2222222222222222222
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <p class="vjCTAheading">About Me</p>
                    <p>{{$data->about_me}}</p>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                    $i = 0 ;
                @endphp
                @foreach ($skills as $item)
                <p class="vjCTAheading">Skill {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->svalue}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>  
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                $i = 0 ;
                @endphp
                @foreach ($achievements as $item)
                <p class="vjCTAheading">Milestone {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->svalue}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                $i = 0 ;
                @endphp
                @foreach ($testimonies as $item)
                <p class="vjCTAheading">Testimonial {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->sduration}}</p>
                        <p>{{$item->sdesp}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                $i = 0 ;
                @endphp
                @foreach ($experiens as $item)
                <p class="vjCTAheading">Experience {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->sduration}}</p>
                        <p>{{$item->sdesp}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                $i = 0 ;
                @endphp
                @foreach ($educations as $item)
                <p class="vjCTAheading">Education {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->sduration}}</p>
                        <p>{{$item->sdesp}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="myModal9" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 999999999
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal10" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal</h5>
                </div>
                <div class="modal-body p-4" id="result">
                    <div>
                        <p>The grid inside the modal is responsive too.. 101010101010
                        </p>
                    </div>
                </div>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="myModal11" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                @php
                $i = 0 ;
                @endphp
                @foreach ($products as $item)
                <p class="vjCTAheading">Product {{++$i}} :</p>
                        <p>{{$item->sname}}</p>
                        <p>{{$item->svalue}}</p>
                @endforeach
                <div class="text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="qrcodeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-full" role="document">
            <div class="modal-content">
                <p class="vjCTAheading">QR Code</p>
                    <p>{!! QrCode::size(300)->generate(url()->current())!!}</p>
                <div class="modal-footer text-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close Me!</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    @include('notify::messages')
    @notifyJs

</body>

</html>