<html>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>How to Crop And Upload Image in Laravel 6 using jQuery Ajax</title>
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('asset/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('asset/css/croppie.css') }}" />
  <!--<script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>!-->
    <script src="{{ asset('asset/js/croppie.js') }}"></script>
 </head>
 <body>

<h1>Add New Product - Step 2</h1>
    <hr>
    @if(isset($personinfo->profile_img))
    Profile Image:
    <img src="{{ asset('/storage/app/crop_image/'.$personinfo->profile_img) }}" />
    <img alt="Profile Image" src="{{ 'storage/app/crop_image/'.$personinfo->profile_img }}"/>
    <img data-src= "{{Storage::get('public/images/', $personinfo->profile_img) }}" alt="Card image cap">
    @endif
  <div class="container">    
      <div class="card">
        <div class="card-header">Crop and Upload Image</div>
        <div class="card-body">
          <div class="form-group">
            @csrf
            <div class="row">
              <div id="image-preview_class" class="col-md-4" style="border-right:1px solid #ddd; display:none">
                <div id="image-preview"></div>
              </div>
              <div id="uploaded_image_class" class="col-md-4" style="padding:75px;background-color: #333 display:none">
                <div id="uploaded_image" align="center" ></div>
              </div>
              <div class="col-md-4" style="padding:75px; border-right:1px solid #ddd;">
                <p><label>Select Image</label></p>
                <input type="file" name="upload_image" id="upload_image" />
                <br />
                <br />
                <button class="btn btn-success crop_image">Crop & Upload Image</button>
              </div>
              
            </div>
          </div>
        </div>
      </div>
      <form action="step2" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <h3>Comapany Logo</h3><br/><br/>
       
        <div class="form-group">
            <input type="file" {{ (!empty($personinfo->profile_img)) ? "disabled" : ''}} class="form-control-file" name="profile_img" id="profile_img" aria-describedby="fileHelp">
            <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
        </div>
        <button type="submit" class="btn btn-primary">Review Product Details</button>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form><br/>
    @if(isset($personinfo->profile_img))
    <form action="remove-image" method="post">
        {{ csrf_field() }}
    <button type="submit" class="btn btn-danger">Remove Image</button>
    </form>
    @endif

<a type="button" href="step1" class="btn btn-warning">Back to Step 1</a>
</body>
</html>



<script type="text/javascript">

$(document).ready(function(){
  
    // $('#image-preview_class').show();
    // $('#uploaded_image_class').hide();
    
  $image_crop = $('#image-preview').croppie({
    enableExif:true,
    viewport:{
      width:200,
      height:200,
      type:'circle'
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').change(function(){
    var reader = new FileReader();

    reader.onload = function(event){
      $image_crop.croppie('bind', {
        url:event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type:'canvas',
      size:'viewport'
    }).then(function(response){
      var _token = $('input[name=_token]').val();
      $.ajax({
        url:'{{ route("image_crop.upload") }}',
        type:'post',
        data:{"image":response, _token:_token},
        dataType:"json",
        success:function(data)
        {
          var crop_image = '<img src="'+data.path+'" />';
          $('#uploaded_image').html(crop_image);
        //   $('#image-preview').html(crop_image);
          $('#image-preview_class').hide();
          $('#uploaded_image_class').show();

        },error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
      });
    });
  });
  
});  
</script>

