@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" id="registerForm">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Mobile No') }}</label>

                            <div class="col-md-6">
                                <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile">
                                <span id="error_mobile"></span>
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <div id="otp_div" style="display: none">
                                    <input id="mobile_otp" type="text" class="form-control" name="mobile_otp" value="{{ old('mobile_otp') }}" autocomplete="mobile_otp">
                                    <button id="otp_btn">Submit</button>
                                </div>
                            </div>
                            {{-- <button class="btn btn-primary" id="btn_verify">Verify</button> --}}
                                
                        </div>

                        
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="subdomain" class="col-md-4 col-form-label text-md-right">{{ __('Subdomain') }}</label>

                            <div class="col-md-6">
                                <input id="subdomain" type="text" class="form-control" name="subdomain" required autocomplete="subdomain">
                                <span id="error_subdomain"></span>
                            </div>.easycards.in
                            
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4" >
                                <button type="submit" class="btn btn-primary" id="register">
                                    {{ __('Register') }}
                                </button>
                                {{-- <button class="btn btn-primary" onclick="$('#registerForm').submit(); return false;" id="formSubmit">{{ __('Register') }}
                                </button> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).ready(function(){

        // $( '#registerForm' ).on('submit', function() {
        // return false;
        // });

        $('#subdomain').blur(function(){
        var error_subdomain = '';
        var subdb = $('#subdomain').val();
        var subdomain = subdb + '.easycards.in';
        var _token = $('input[name="_token"]').val();
        var filter =    /^([a-zA-Z0-9]([-a-zA-Z0-9]{0,61}[a-zA-Z0-9])?\.)?([a-zA-Z0-9]{1,2}([-a-zA-Z0-9]{0,252}[a-zA-Z0-9])?)\.([a-zA-Z]{2,63})$/;
        if(!filter.test(subdomain))
        {    
        $('#error_subdomain').html('<label class="text-danger">Invalid Subdomain</label>');
        $('#subdomain').addClass('has-error');
        $('#register').attr('disabled', 'disabled');
        }
        else
        {
        $.ajax({
        url:"{{ route('subdomain.check') }}",
        method:"POST",
        data:{subdomain:subdb, _token:_token},
        success:function(result)
        {
            if(result == 'unique')
            {
            $('#error_subdomain').html('<label class="text-success">Subdomain Available</label>');
            $('#subdomain').removeClass('has-error');
            $('#register').attr('disabled', false);
            }
            else
            {
            $('#error_subdomain').html('<label class="text-danger">Subdomain not Available</label>');
            $('#subdomain').addClass('has-error');
            $('#register').attr('disabled', 'disabled');
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        })
        }
        });

        $('#btn_verify').click(function(){
            var btn_value = $(this).text();
            
            var error_mobile = '';
            var mobile = $('#mobile').val();
            var _token = $('input[name="_token"]').val();
            var filter = /^(0|[+91]{3})?[7-9][0-9]{9}$/;

            $('#mobile_otp').val('');
            
            if(btn_value == 'Verify'){
                if(!filter.test(mobile) || mobile.length < 10)
                    {    
                    $('#error_mobile').html('<label class="text-danger">Invalid Mobile</label>');
                    $('#mobile').addClass('has-error');
                    $('#otp_div').css('display','none');
                    $('#otp_div').attr('disabled', 'disabled');
                    }else{
                        checkMobile(mobile,'error_mobile',_token);
                    }
            }else if(btn_value='Resend Otp'){
                SendOtp(mobile,_token);  
            }
            return false;
        });

        $('#mobile').blur(function(){
        var error_mobile = '';
        
        var mobile = $('#mobile').val();
        var _token = $('input[name="_token"]').val();
        var filter = /^(0|[+91]{3})?[7-9][0-9]{9}$/;
        

        if(!filter.test(mobile))
        {    
        $('#error_mobile').html('<label class="text-danger">Invalid Mobile</label>');
        $('#mobile').addClass('has-error');
        $('#otp_div').css('display','none');
        $('#otp_div').attr('disabled', 'disabled');
        }
        else{
            checkMobile(mobile,'error_mobile',_token);
        }
        });
        
        
        /*$('#otp_btn').click(function(){
        var error_mobile = '';
        var mobile_otp = $('#mobile_otp').val();
        var _token = $('input[name="_token"]').val();
        
        $.ajax({
        url:"{{ route('verify.otp') }}",
        method:"POST",
        data:{otp:mobile_otp, _token:_token},
        success:function(result)
        {
            $data = JSON.parse(result);
            if($data.error == 0)
            {
            $('#error_mobile').html('<label class="text-success">'+$data.message+'</label>');
            $('#mobile').removeClass('has-error');
            $('#otp_div').css('display','none');
            $('#otp_div').attr('disabled', 'disabled');
            $('#mobile').attr('disabled', 'disabled');
            $('#btn_verify').attr('disabled', 'disabled');
            }
            else
            {
            $('#error_mobile').html('<label class="text-danger">'+$data.message+'</label>');
            $('#mobile').addClass('has-error');
            $('#otp_div').css('display','block');
            $('#btn_verify').html('Resend Otp');
            $('#otp_div').attr('disabled', false);
            $('#mobile').attr('disabled', false);
            }
            
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        })
        return false;

        });
        */
        function checkMobile(mobile,spanid,_token){
            
            var error_span = '#'+spanid;
            var return_value;
            $.ajax({
            url:"{{ route('mobile.check') }}",
            method:"POST",
            data:{mobile:mobile, _token:_token},
            success:function(result)
            {
                if(result == 'unique')
                {
                $(error_span).html('<label class="text-success">Mobile Available</label>');
                $('#mobile').removeClass('has-error');
                SendOtp(mobile,_token);
                }
                else
                {
                $(error_span).html('<label class="text-danger">Mobile No Already Registed with Us.</label>');
                $('#mobile').addClass('has-error');
                $('#otp_div').css('display','none');
                $('#otp_div').attr('disabled', 'disabled');
                
                }
               
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
                }
            })
            
            }
        
        function SendOtp(mobile,_token){
            $.ajax({
            url:"{{ route('send.otp') }}",
            method:"POST",
            data:{mobile:mobile, _token:_token},
            success:function(result)
            {
                $data = JSON.parse(result);
                if($data.error == 0)
                {
                $('#error_mobile').html('<label class="text-success">'+$data.message+'-'+$data.OTP+'</label>');
                $('#mobile').removeClass('has-error');
                $('#otp_div').css('display','block');
                $('#mobile').attr('disabled', 'disabled');
                }
                else
                {
                $('#error_mobile').html('<label class="text-danger">'+$data.message+'</label>');
                $('#mobile').addClass('has-error');
                $('#mobile').attr('disabled', false);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR, textStatus, errorThrown);
                }
            })
        }
        
        });

</script>
@endsection
