@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Testimonial List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('testimonial.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Tag Line</th>
            <th>Message</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($testimonials as $testimonial)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $testimonial->sname }}</td>
            <td>{{ $testimonial->sduration }}</td>
            <td>{{ $testimonial->sdesp }}</td>
            <td>
                <form action="{{ route('testimonial.destroy',$testimonial->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('testimonial.show',$testimonial->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('testimonial.edit',$testimonial->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $testimonials->links() !!}
      
@endsection