@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Referral List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('referral.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Referral Name</th>
            <th>Referral Mobile No</th>
            <th>Is Register</th>
            <th>Register By</th>
            <th>Is Subscribed</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($referrals as $referral)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $referral->sname }}</td>
            <td>{{ $referral->svalue }}</td>
            <td>{{ ($referral->is_register == 1) ? 'YES':'Not Yet'}}</td>
            <td>{{ $referral->by_whom }}</td>
            <td>{{ ($referral->is_subscrib == 1) ? 'YES':'Not Yet' }}</td>
            <td>
                <form action="{{ route('referral.destroy',$referral->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('referral.show',$referral->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('referral.edit',$referral->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {{-- {!! $referrals->links() !!} --}}
      
@endsection