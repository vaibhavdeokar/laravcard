@extends('layouts.app')

@section('content')
    <h1>Add Personal info - Step 1</h1>
    <hr>
     {{-- {{dd($personinfo->first_name)}} --}}
    <form action="/step1" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">First Name</label>
            <input type="text" value="{{ isset($personinfo->first_name) ? $personinfo->first_name : '' }}" class="form-control" id="first_name"  name="first_name">
        </div>
        <div class="form-group">
            <label for="title">Last Name</label>
            <input type="text" value="{{ isset($personinfo->last_name) ? $personinfo->last_name : '' }}" class="form-control" id="last_name"  name="last_name">
        </div>
        {{--
        <div class="form-group">
            <label for="title">Profession</label>
            <input type="text" value="{{{ $personinfo->profession or '' }}}" class="form-control" id="profession"  name="profession">
        </div>
        <div class="form-group">
            <label for="title">Website</label>
            <input type="text" value="{{{ $personinfo->website or '' }}}" class="form-control" id="website"  name="website">
        </div>
        <div class="form-group">
            <label for="title">Product Name</label>
            <input type="text" value="{{{ $personinfo->name or '' }}}" class="form-control" id="taskTitle"  name="name">
        </div>
        {{-- <div class="form-group">
            <label for="description">Product Company</label>
            <select class="form-control" name="company">
                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Apple') ? "selected=\"selected\"" : "" }}}>Apple</option>
                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Google') ? "selected=\"selected\"" : "" }}}>Google</option>
                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Mi') ? "selected=\"selected\"" : "" }}}>Mi</option>
                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Samsung') ? "selected=\"selected\"" : "" }}}>Samsung</option>
            </select>
        </div> --}}
        {{-- <div class="form-group">
            <label for="description">Mobile</label>
            <input type="text"  value="{{{ $personinfo->mobile or '' }}}" class="form-control" id="mobile" name="mobile"/>
        </div> --}}
        {{-- <div class="form-group">
            <label for="description">Product Available</label><br/>
            <label class="radio-inline"><input type="radio" name="available" value="1" {{{ (isset($personinfo->available) && $personinfo->available == '1') ? "checked" : "" }}}> Yes</label>
            <label class="radio-inline"><input type="radio" name="available" value="0" {{{ (isset($personinfo->available) && $personinfo->available == '0') ? "checked" : "" }}}> No</label>
        </div> 
        <div class="form-group">
            <label for="description">About Me</label>
            <textarea type="text"  class="form-control" id="about_me" name="about_me">{{{ $personinfo->about_me or '' }}}</textarea>
        </div>--}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <button type="submit" class="btn btn-primary">Add Profile Image</button>
    </form>
@endsection
