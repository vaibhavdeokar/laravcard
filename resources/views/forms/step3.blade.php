@extends('layouts.app')

@section('content')
    <h1>Add New Person - Step 3</h1>
    <hr>
    <h3>Review Person Details</h3>
    <form action="store" method="post" >
        {{ csrf_field() }}
        <table class="table">
            <tr>
                <td>Product First Name:</td>
                <td><strong>{{$personinfo->first_name}}</strong></td>
            </tr>
            <tr>
                <td>Product Amount:</td>
                <td><strong>{{$personinfo->last_name}}</strong></td>
            </tr>
            <tr>
                <td>Product Company:</td>
                <td><strong>{{$personinfo->first_name}}</strong></td>
            </tr>
            
            <tr>
                <td>Product Description:</td>
                <td><strong>{{$personinfo->first_name}}</strong></td>
            </tr>
            <tr>
                <td>Profile Image:</td>
                <td><strong><img alt="Product Image" src="{{ asset('/storage/crop_image/'.$personinfo->profile_img) }}" width="100" height="100"/></strong></td>
            </tr>
            <tr>
                <td>Company Image:</td>
                <td><strong><img alt="Company Image" src="{{ asset('/storage/crop_image/'.$personinfo->company_logo) }}" width="200" height="150"/></strong></td>
            </tr>
        </table>
        <a type="button" href="step1" class="btn btn-warning">Back to Step 1</a>
        <a type="button" href="step2" class="btn btn-warning">Back to Step 2</a>
        <button type="submit" class="btn btn-primary">Create Product</button>
    </form>
@endsection