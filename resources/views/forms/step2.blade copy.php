

@extends('layouts.app')

@section('content')

<h1>Add New Product - Step 2</h1>
    <hr>
    @if(isset($personinfo->profile_img))
   
    Product Image:
    <img src="{{ asset('/storage/app/crop_image/'.$personinfo->profile_img) }}" />
    <img alt="Profile Image" src="{{ 'storage/app/crop_image/'.$personinfo->profile_img }}"/>
    @endif
    <form action="step2" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <h3>Upload Product Image</h3><br/><br/>

        <div class="form-group">
            <input type="file" {{ (!empty($personinfo->profile_img)) ? "disabled" : ''}} class="form-control-file" name="profile_img" id="profile_img" aria-describedby="fileHelp">
            <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
        </div>
        <button type="submit" class="btn btn-primary">Review Product Details</button>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </form><br/>
    @if(isset($personinfo->profile_img))
    <form action="remove-image" method="post">
        {{ csrf_field() }}
    <button type="submit" class="btn btn-danger">Remove Image</button>
    </form>
    @endif

<a type="button" href="step1" class="btn btn-warning">Back to Step 1</a>
        
@endsection