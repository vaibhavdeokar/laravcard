@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Education Details List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('education.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Duration</th>
            <th>Description</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($educations as $education)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $education->sname }}</td>
            <td>{{ $education->sduration }}</td>
            <td>{{ $education->sdesp }}</td>
            <td>
                <form action="{{ route('education.destroy',$education->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('education.show',$education->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('education.edit',$education->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $educations->links() !!}
      
@endsection