@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Skills List</h2>
            </div>
            
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Color type</th>
            <th>Active</th>
            <th>Value</th>
            <th>Actual Color</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($colors as $skill)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $skill->sname }}</td>
            <td>{{ $skill->svalue }}</td>
            <td><input type="checkbox" value="1" {{  ($skill->is_active == 1 ? ' checked' : '') }} disabled="disabled" ></td>
            <td style="background-color:{{ $skill->svalue }}"> </td>
            <td>
                 <a class="btn btn-primary" href="{{ route('colorschema.edit',$skill->id) }}">Edit</a>
            </td>
        </tr>
        @endforeach
    </table>

      
@endsection