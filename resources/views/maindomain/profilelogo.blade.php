<html>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Profile Pricture</title>
  <script src="{{ asset('asset/js/jquery.min.js') }}"></script>
  <link rel="stylesheet" href="{{ asset('asset/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('asset/css/croppie.css') }}" />
    <script src="{{ asset('asset/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('asset/js/croppie.js') }}"></script>
 </head>
 <body> 

    {{-- @if(isset($pinfo->profile_logo)) --}}
    {{-- Profile Image:
    <img src="{{ asset('/storage/crop_image/'.$pinfo->profile_logo) }} " /> --}}
    {{-- @else --}}
    <div class="container">    
        <div class="card-header">Profile Image</div>

            <div class="form-group">
                @csrf
                <div class="row">
                <div id="image-preview_class" class="col-md-4" style="border-right:1px solid #ddd; display:none">
                    <div id="image-preview"></div>
                </div>
                <div id="uploaded_image_class" class="col-md-4" style="padding:75px;background-color: #333 display:none">
                    <div id="uploaded_image" align="center" ></div>
                </div>
                <div class="" id="profile_data" style="padding:75px; border-right:1px solid #ddd;">
                    <input type="file" name="upload_image" id="upload_image" />
                    <button class="btn btn-success crop_image" style="display:none">Crop & Upload Image</button>
                </div>
                <div style="display:none" id="profilelabel">
                    <label for="Profile">Profile Uploaded Successfully</label>
                </div>
            </div>
        </div>
        <div id="profile_logo">
            Profile Image:
            <img src="{{ asset('/storage/userdata/'.$pinfo->profile_logo) }} " />
            
        </div>
        
    </div>
    {{-- @endif --}}

</body>
</html>



<script type="text/javascript">

$(document).ready(function(){
  
    // $('#image-preview_class').show();
    // $('#uploaded_image_class').hide();
    
  $image_crop = $('#image-preview').croppie({
    enableExif:true,
    viewport:{
      width:200,
      height:200,
      type:'circle'
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#upload_image').change(function(){
    var reader = new FileReader();

    reader.onload = function(event){
      $image_crop.croppie('bind', {
        url:event.target.result
      }).then(function(){
        $('#image-preview_class').show();
        $('#profile_logo').hide();
        $('.crop_image').show();
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type:'canvas',
      size:'viewport'
    }).then(function(response){
      var _token = $('input[name=_token]').val();
      $.ajax({
        url:'{{ route("profilelogo") }}',
        type:'post',
        data:{"image":response, _token:_token},
        dataType:"json",
        success:function(data)
        {
          var crop_image = '<img src="'+data.path+'" />';
          $('#uploaded_image').html(crop_image);
        //   $('#image-preview').html(crop_image);
          $('#image-preview_class').hide();
          $('#profile_data').hide();
          
          $('#uploaded_image_class').show();
          $('#profilelabel').show();

        },error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
      });
    });
  });
  
});  
</script>


