@extends('layouts.app')

@section('content')
<!-- Style to create button -->
<style> 
    .GFG { 
        background-color: white; 
        border: 2px solid black; 
        color: green; 
        padding: 5px 10px; 
        text-align: center; 
        display: inline-block; 
        font-size: 20px; 
        margin: 10px 30px; 
        cursor: pointer; 
        text-decoration:none; 
    } 
</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                {{--    You are logged in!
                    Your in Main Domain

                </div>
                <div id="redirect"></div>
            <form action="{{route('dashboard')}}" method="post">
                @csrf
                <button type="submit">Go to Dashboard</button>
            </form>
        <button onclick="OpenWindow('{{$data}}')">Go to Subdomain</button>
            </div>
            --}}
            <a class="btn btn-primary col-md-3" href="{{ route('skills.index') }}"> Skills</a>
            <a class="btn btn-primary col-md-3" href="{{ route('achievements.index') }}"> Achievements</a>
            <a class="btn btn-primary col-md-3" href="{{ route('testimonial.index') }}"> Testimonials</a>
            <a class="btn btn-primary col-md-3" href="{{ route('experience.index') }}"> Experience</a>
            <a class="btn btn-primary col-md-3" href="{{ route('education.index') }}"> Education</a>
            <a class="btn btn-primary col-md-3" href="{{ route('product.index') }}"> Product</a>
            <a class="btn btn-primary col-md-3" href="{{ route('payment.index') }}"> Payment Link</a>
            <a class="btn btn-primary col-md-3" href="{{ route('social.index') }}"> Social Link</a>
            <a class="btn btn-primary col-md-3" href="{{ route('userinfo3') }}"> Company</a>
            <a class="btn btn-primary col-md-3" href="{{ route('userinfo2') }}"> Contact Details</a>
            <a class="btn btn-primary col-md-3" href="{{ route('userinfo1') }}"> About Me</a>
            <a class="btn btn-primary col-md-3" href="{{ route('companylogo') }}"> Company Logo</a>
            <a class="btn btn-primary col-md-3" href="{{ route('profilelogo') }}"> Profile Picture</a>
            <a class="btn btn-primary col-md-3" href="{{ route('referral.index') }}"> Referral List</a>
            <a class="btn btn-primary col-md-3" href="{{ route('password.change') }}"> Change Password</a>
            <a class="btn btn-primary col-md-3" href="{{ route('colorschema.index') }}"> Color Schema</a>
            <a class="btn btn-primary col-md-3" href="{{ route('images.index') }}"> Image Gallary</a>
            <a class="btn btn-primary col-md-3" href="{{ route('location.index') }}"> Locations</a>

            <button class="btn btn-primary col-md-12" onclick="OpenWindow('{{$data['subdomain_url']}}')">Go to Subdomain</button>
            <label for="url">Referral URL = {{$data['referral_url']}}</label>
            <br>
        <label for="count">Referal Count = {{$data['referral_count']}}</label>
            </div>
    </div>
</div>

<script>
    function OpenWindow(url){
        window.open(url, '_blank');
    }

</script>

@endsection
