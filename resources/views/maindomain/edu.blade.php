@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Educational Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                    
                    <form action="{{route('edu')}}" method="post">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="title">input 1</label>
                            <input type="text" value="{{ old('first_name') }}" class="form-control" id="first_name"  name="first_name">
                        </div>
                        <div class="form-group">
                            <label for="title">input 2</label>
                            <input type="text" value="{{ old('last_name') }}" class="form-control" id="last_name"  name="last_name">
                        </div>
                        <div class="form-group">
                            <label for="title">input 3</label>
                            <input type="text" value="{{ old('last_name') }}" class="form-control" id="profession"  name="profession">
                        </div>
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
             </div>
         </div>
    </div>
</div>

    
@endsection
