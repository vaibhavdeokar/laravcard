@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Verify Your Mobile First</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('send.otp') }}">
                        @csrf 
   
                        {{-- @if (session('error'))
                        <div class="alert alert-success" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif --}}
  
                        <div class="form-group row">
                            <label for="otp" class="col-md-4 col-form-label text-md-right">Enter OTP</label>
  
                            <div class="col-md-6">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <input type="hidden" name="mobile" value="{{$user->mobile}}">
                                <input id="otp" type="text" class="form-control" name="otp" >
                            </div>
                        </div>
  
                        {{-- <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>
  
                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>
  
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>
    
                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>
    --}}
           <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Submit Otp
                                </button>
                                <button class="btn btn-primary"  onclick="event.preventDefault();
                                document.getElementById('sendform').submit();">
                                        Resend Otp
                                </button>
                            
                            </div>
                        </div>
                    </form>
                     {{-- @if ($user->mobile_verified == 0) --}}
                     
                     <form id="sendform" action="{{ route('send.otp') }}" method="GET" enctype="multipart/form-data" style="display: none;">
                         @csrf
                     </form>
                 {{-- @endif --}}
                </div>
             </div>
         </div>
    </div>
</div>

@endsection
