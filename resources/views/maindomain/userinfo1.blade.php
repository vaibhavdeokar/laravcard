@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Personal Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('userinfo1')}}" method="post">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="title">First Name</label>
                            <input type="text" value="{{ old('first_name')  ?? isset($pinfo->first_name) ? $pinfo->first_name : ''}}" class="form-control" id="first_name"  name="first_name">
                        </div>
                        <div class="form-group">
                            <label for="title">Last Name</label>
                            <input type="text" value="{{ old('last_name') ?? isset($pinfo->last_name) ? $pinfo->last_name : ''}}" class="form-control" id="last_name"  name="last_name">
                        </div>
                        <div class="form-group">
                            <label for="title">Profession</label>
                            <input type="text" value="{{ old('profession') ?? isset($pinfo->profession) ? $pinfo->profession : ''}}" class="form-control" id="profession"  name="profession">
                        </div>
                        <div class="form-group">
                            <label for="title">Website</label>
                            <input type="url" value="{{ old('website') ?? isset($pinfo->website) ? $pinfo->website : ''}}" class="form-control" id="website"  name="website">
                        </div>
                        <div class="form-group">
                            <label for="title">Mobile</label>
                            <input type="text" value="{{ old('mobile') ?? isset($pinfo->mobile) ? $pinfo->mobile : ''}}" class="form-control" id="mobile"  name="mobile" readonly>
                        </div>
                        {{-- <div class="form-group">
                            <label for="description">Product Company</label>
                            <select class="form-control" name="company">
                                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Apple') ? "selected=\"selected\"" : "" }}}>Apple</option>
                                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Google') ? "selected=\"selected\"" : "" }}}>Google</option>
                                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Mi') ? "selected=\"selected\"" : "" }}}>Mi</option>
                                <option {{{ (isset($personinfo->company) && $personinfo->company == 'Samsung') ? "selected=\"selected\"" : "" }}}>Samsung</option>
                            </select>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="description">Mobile</label>
                            <input type="text"  value="{{{ $personinfo->mobile or '' }}}" class="form-control" id="mobile" name="mobile"/>
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="description">Product Available</label><br/>
                            <label class="radio-inline"><input type="radio" name="available" value="1" {{{ (isset($personinfo->available) && $personinfo->available == '1') ? "checked" : "" }}}> Yes</label>
                            <label class="radio-inline"><input type="radio" name="available" value="0" {{{ (isset($personinfo->available) && $personinfo->available == '0') ? "checked" : "" }}}> No</label>
                        </div>--}}
                        <div class="form-group">
                            <label for="description">About Me</label>
                            <textarea type="text"  class="form-control" id="about_me" name="about_me">{{ old('about_me') ?? isset($pinfo->about_me) ? $pinfo->about_me : ''}}</textarea>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
             </div>
         </div>
    </div>
</div>

    
@endsection
