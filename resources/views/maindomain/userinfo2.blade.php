@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Conatct Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('userinfo2')}}" method="post">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="title">Email</label>
                            <input type="email" value="{{ old('email')  ?? isset($pinfo->email) ? $pinfo->email : ''}}" class="form-control" id="email"  name="email">
                        </div>
                        <div class="form-group">
                            <label for="title">Whatsapp</label>
                            <input type="text" value="{{ old('phone') ?? isset($pinfo->phone) ? $pinfo->phone : ''}}" class="form-control" id="phone"  name="phone">
                        </div>
                        <div class="form-group">
                            <label for="description">Address</label>
                            <textarea type="text"  class="form-control" id="address" name="address">{{ old('address') ?? isset($pinfo->address) ? $pinfo->address : ''}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="title">Skype Id</label>
                            <input type="text" value="{{ old('skype_id') ?? isset($pinfo->skype_id) ? $pinfo->skype_id : ''}}" class="form-control" id="skype_id"  name="skype_id">
                        </div>
                        <div class="form-group">
                            <label for="title">Youtube url About me</label>
                            <input type="text" value="{{ old('youtube_about') ?? isset($pinfo->youtube_about) ? $pinfo->youtube_about : ''}}" class="form-control" id="youtube_about"  name="youtube_about">
                        </div>
                        <div class="form-group">
                            <label for="title">Fn Messanger Url</label>
                            <input type="text" value="{{ old('fb_msg') ?? isset($pinfo->fb_msg) ? $pinfo->fb_msg : ''}}" class="form-control" id="fb_msg"  name="fb_msg">
                        </div>
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
             </div>
         </div>
    </div>
</div>

    
@endsection
