@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Company Logo</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('companylogo')}}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                    <div class="form-group">
                        <input id="file-upload" type="file" name="company_logo" accept="image/*" onchange="readURL(this);">
                        <label for="file-upload" id="file-drag">
                            <img id="file-image" src="#" alt="Preview" class="hidden">
                            <div id="start" >
                                <i class="fa fa-download" aria-hidden="true"></i>
                                <div>Select a file or drag here</div>
                                <div id="notimage" class="hidden">Please select an image</div>
                                <span id="file-upload-btn" class="btn btn-primary">Select a file</span>
                                <br>
                                <span class="text-danger">{{ $errors->first('company_logo') }}</span>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </label>
                    </div>
                        
                        
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                    </form>
                    Design Reference = https://www.tutsmake.com/laravel-example/l/image
                </div>
             </div>
         </div>
    </div>
</div>

<script type="text/javascript">

    // $(document).ready(function(){

    function readURL(input, id) {

      id = id || '#file-image';

      if (input.files && input.files[0]) {
          var reader = new FileReader();
  
          reader.onload = function (e) {
              $(id).attr('src', e.target.result);
          };
  
          reader.readAsDataURL(input.files[0]);
          $('#file-image').removeClass('hidden');
          $('#start').hide();
      }
   }

    // });
  </script> 
    
@endsection
