@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Comapny Information</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{route('userinfo3')}}" method="post">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="title">Comapny Name</label>
                            <input type="text" value="{{ old('cname')  ?? isset($pinfo->cname) ? $pinfo->cname : ''}}" class="form-control" id="cname"  name="cname">
                        </div>
                        <div class="form-group">
                            <label for="title">Comapny Tag Line</label>
                            <input type="text" value="{{ old('ctagline') ?? isset($pinfo->ctagline) ? $pinfo->ctagline : ''}}" class="form-control" id="ctagline"  name="ctagline">
                        </div>
                        <div class="form-group">
                            <label for="description">About Comapny</label>
                            <textarea type="text"  class="form-control" id="cabout" name="cabout">{{ old('cabout') ?? isset($pinfo->cabout) ? $pinfo->cabout : ''}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="title">Youtube link about company</label>
                            <input type="text" value="{{ old('youtube_company') ?? isset($pinfo->youtube_company) ? $pinfo->youtube_company : ''}}" class="form-control" id="youtube_company"  name="youtube_company">
                        </div>
                        <div class="form-group">
                            <label for="title">Map Link</label>
                            <input type="text" value="{{ old('cmap') ?? isset($pinfo->cmap) ? $pinfo->cmap : ''}}" class="form-control" id="cmap"  name="cmap">
                        </div>
                        
                        
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>

                </div>
             </div>
         </div>
    </div>
</div>

    
@endsection
