@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Achievements List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('achievements.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Achievement Name</th>
            <th>Achievement Value</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($achievements as $achievement)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $achievement->sname }}</td>
            <td>{{ $achievement->svalue }}</td>
            <td>
                <form action="{{ route('achievements.destroy',$achievement->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('achievements.show',$achievement->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('achievements.edit',$achievement->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $achievements->links() !!}
      
@endsection