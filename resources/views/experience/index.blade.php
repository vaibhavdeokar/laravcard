@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Experience List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('experience.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Duration</th>
            <th>Description</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($experiences as $experience)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $experience->sname }}</td>
            <td>{{ $experience->sduration }}</td>
            <td>{{ $experience->sdesp }}</td>
            <td>
                <form action="{{ route('experience.destroy',$experience->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('experience.show',$experience->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('experience.edit',$experience->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $experiences->links() !!}
      
@endsection