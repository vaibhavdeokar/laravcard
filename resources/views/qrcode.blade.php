<html>
    <head>
    </head>
        <style>
                .center-screen {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            min-height: 100vh;
            } 
        </style>
    <body>
        <div class="center-screen">
            {{$url}}
            {!! QrCode::size(500)->generate($url)!!}
        </div>
    </body>
 </html>
