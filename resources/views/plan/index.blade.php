@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Plan List</h2>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   {{$data}}
    <div class="container">
        @foreach ($plans as $plan)
            <div class="row">
                <div class="col-lg-12 margin-tb">
                    <div class="pull-left">
                        <h2>{{$plan->name.'<==>'.$plan->slug}}</h2>
                    </div>
                    @if ($plan->id == $data->plan_id && $data->is_active == true)
                        <div class="pull-right">
                        <form action="{{route('subscription.store')}}" method="post">
                            @csrf
                                <input type="hidden" name="id" value="{{$plan->id}}">
                                <input type="hidden" name="sub_id" value="{{$data->id}}">
                                <input type="hidden" name="sub_name" value="unsubscribe">
                                <input type="hidden" name="slug" value="{{$data->slug}}">
                                <button type="submit" class="btn btn-success">UnSubscribe</button>
                                <button type="button" class="btn btn-success" onclick="changePlan('unsubscribe');">UnSubscribe</button>
                            </form>
                        </div>
                    @elseif ($plan->id == $data->plan_id && $data->is_ended == true)
                            <div class="pull-right">
                                <form action="{{route('subscription.store')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$plan->id}}">
                                    <input type="hidden" name="sub_name" value="renew">
                                    <input type="hidden" name="amount" value="{{$plan->price}}">
                                    <input type="hidden" name="slug" value="{{$data->slug}}">
                                    <button type="submit" class="btn btn-success">Renew</button>
                                    <button type="submit" class="btn btn-success" onclick="payment(this);" data-amount="{{$plan->price}}" data-planid="{{$plan->id}}" data-planslug="{{$plan->slug}}">Renew</button>
                                </form>
                            </div>
                        @else
                        <div class="pull-right">
                            <form action="{{route('pay_payment')}}" method="get">
                            <input type="hidden" name="id" value="{{$plan->id}}">
                                <input type="hidden" name="amount" value="{{$plan->price}}">
                                <input type="hidden" name="slug" value="{{$plan->slug}}">
                                <input type="hidden" name="">
                                <button type="submit" class="btn btn-success">SubScribe To Plan</button>
                                <button type="button" class="btn btn-success" onclick="payment(this);" data-amount="{{$plan->price}}" data-planid="{{$plan->id}}" data-planslug="{{$plan->slug}}">SubScribe To Plan Razor</button>
                            </form>
                        </div>
                    @endif
                  
                </div>
            </div>
        @endforeach
    </div>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>

    function payment(plan){

        // var amount = getPlansDetails('plan_FNRbRXj2UqXeFA');
        // return
        var totalAmount = $(plan).attr("data-amount");
        var planid = $(plan).attr("data-planid");
        var planslug = $(plan).attr("data-planslug");
        var options = {
        key: "{{ Config::get('razor.razor_key') }}",
        amount: totalAmount * 100,
        handler: function (response){
            demoSuccessHandler(response,planid,planslug);
        },
        "prefill": {
               "contact": "{{\Auth::user()->mobile}}",
               "email":   "{{\Auth::user()->email}}",
           },
    }

    window.r = new Razorpay(options);
    r.open();
    
    }

    function demoSuccessHandler(transaction,planid,planslug) {
        // console.log(transaction);
        $.ajax({
            method: 'post',
            url: "{!!route('razorpayment')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "payment_id": transaction.razorpay_payment_id,
                "plan_id":planid
            },
            success: function (msg) {
                data = JSON.parse(JSON.stringify(msg));
                if(data.status){
                    location.reload()
                }else{
                    alert('Something Went Wrong');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            },
            complete: function (r) {
                console.log('complete');
                console.log(r);
            }
        })
    }

    function changePlan(subname){
        $.ajax({
            method: 'post',
            url: "{!!route('subscription.store')!!}",
            data: {
                "_token": "{{ csrf_token() }}",
                "sub_name": subname,
            },
            success: function (msg) {
                data = JSON.parse(JSON.stringify(msg));
                if(data.status){
                    location.reload()
                }else{
                    alert('Something Went Wrong');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        })
    }
</script>
      
@endsection