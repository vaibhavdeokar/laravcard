@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Paymenet Link List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('payment.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Payment Name</th>
            <th>Payment Link</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($payments as $payment)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $payment->sname }}</td>
            <td>{{ $payment->svalue }}</td>
            <td>
                <form action="{{ route('payment.destroy',$payment->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('payment.show',$payment->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('payment.edit',$payment->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $payments->links() !!}
      
@endsection