@extends('layouts.app')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Location List</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('location.create') }}">Add</a>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Location Name</th>
            <th>Location Add</th>
            <th>Location Mobile</th>
            <th>Location Email</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($location as $item)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $item->lname }}</td>
            <td>{{ $item->ladd }}</td>
            <td>{{ $item->lmobile }}</td>
            <td>{{ $item->lmail }}</td>
            <td>
                <form action="{{ route('location.destroy',$item->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('location.show',$item->id) }}">Show</a>
    
                    <a class="btn btn-primary" href="{{ route('location.edit',$item->id) }}">Edit</a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $location->links() !!}
      
@endsection