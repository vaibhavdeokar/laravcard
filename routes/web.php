<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Auth::routes();

Route::group(['domain' => \Config::get('app.base_url')], function () {

    Auth::routes();

    Route::get('/', function () {
        return view('landing');
    });
    Route::get('/health', function () {
        response()->json(['success' => 'success'], 200);
    });
    Route::get('/users', function () {
        // $users = \DB::connection('maindb')->table('users')->get();
        $users = \App\User::find(14);
        return $users;
    });

    Route::get('home', 'MainDomain\HomeController@index')->name('home.maindomain');
    Route::post('dashboard', ['as' => 'dashboard', 'uses' => 'MainDomain\HomeController@dashboard']);

    Route::post('/subdomain/check', 'Home\SubdomainCheckController@check')->name('subdomain.check');
    Route::post('/mobile/check', 'Home\SubdomainCheckController@check')->name('mobile.check');
    // Route::post('/sendotp', 'Home\SubdomainCheckController@sendOtp')->name('send.otp');
    Route::post('/verifyotp', 'Home\SubdomainCheckController@verifyOtp')->name('verify.otp');
    Route::get('/sendotp', 'MainDomain\HomeController@sendOtp')->name('send.otp');
    Route::post('/sendotp', 'MainDomain\HomeController@verifyOtp')->name('send.otp');
    Route::post('/verify', 'Home\VerifyController@verifyMobile')->name('verify');

    Route::get('userinfo', ['as' => 'userinfo', 'uses' => 'MainDomain\UserinfoController@index']);
    Route::post('userinfo', ['as' => 'userinfo', 'uses' => 'MainDomain\UserinfoController@storePersonalinfo']);

    Route::get('userinfo1', ['as' => 'userinfo1', 'uses' => 'MainDomain\UserinfoController@index1']);
    Route::post('userinfo1', ['as' => 'userinfo1', 'uses' => 'MainDomain\UserinfoController@storeinfo1']);

    Route::get('userinfo2', ['as' => 'userinfo2', 'uses' => 'MainDomain\UserinfoController@index2']);
    Route::post('userinfo2', ['as' => 'userinfo2', 'uses' => 'MainDomain\UserinfoController@storeinfo2']);

    Route::get('userinfo3', ['as' => 'userinfo3', 'uses' => 'MainDomain\UserinfoController@index3']);
    Route::post('userinfo3', ['as' => 'userinfo3', 'uses' => 'MainDomain\UserinfoController@storeinfo3']);

    Route::get('companylogo', ['as' => 'companylogo', 'uses' => 'MainDomain\UserinfoController@companylogo']);
    Route::post('companylogo', ['as' => 'companylogo', 'uses' => 'MainDomain\UserinfoController@storecompanylogo']);

    Route::get('profilelogo', ['as' => 'profilelogo', 'uses' => 'MainDomain\UserinfoController@profilelogo']);
    Route::post('profilelogo', ['as' => 'profilelogo', 'uses' => 'MainDomain\UserinfoController@storeprofilelogo']);

    Route::get('password/change', ['as' => 'password.change', 'uses' => 'MainDomain\UserinfoController@passwordreset']);
    Route::post('password/change', ['as' => 'password.change', 'uses' => 'MainDomain\UserinfoController@storepassword']);

// Route::post('userinfo', ['as' => 'userinfo', 'uses' => 'MainDomain\UserinfoController@storePersonalinfo']);

    Route::resource('skills', 'MainDomain\SkillController');
    Route::resource('achievements', 'MainDomain\AchievementController');
    Route::resource('product', 'MainDomain\ProductController');
    Route::resource('payment', 'MainDomain\PaymentController');
    Route::resource('social', 'MainDomain\SocialController');
    Route::resource('experience', 'MainDomain\ExperienceController');
    Route::resource('education', 'MainDomain\EducationController');
    Route::resource('testimonial', 'MainDomain\TestimonialController');
    Route::resource('subscription', 'MainDomain\SubscriptionController');
    Route::resource('referral', 'MainDomain\ReferralController');
    Route::resource('plan', 'MainDomain\PlanController');
    Route::resource('colorschema', 'MainDomain\ColorController');
    Route::resource('images', 'MainDomain\ImageGalleryController');
    Route::resource('location', 'MainDomain\LocationController');

    Route::get('plansub', ['as' => 'plansub', 'uses' => 'MainDomain\PlanController@CustomSubscribe']);

    Route::resource('preview', 'MainDomain\PreviewController');

    Route::get('clear_cache', function () {
        \Artisan::call('optimize:clear');
        return "Optimize is cleared";
    });

    Route::get('paypayment', ['as' => 'pay_payment', 'uses' => 'MainDomain\PayumoneyController@payment']);

    Route::get('paypayment/status', ['as' => 'pay_payment.status', 'uses' => 'MainDomain\PayumoneyController@status']);

    // Get Route For Show Payment Form
    Route::get('paywithrazorpay', 'RazorpayController@payWithRazorpay');
    Route::post('paysuccess', 'RazorpayController@paysuccess');
    Route::post('makesubscription', ['as' => 'makesubscription', 'uses' => 'RazorpayController@makeSubscription']);
    Route::post('razor-thank-you', 'RazorpayController@thankYou');

    // route for to show payment form using get method
    Route::get('pay', 'RazorpayController@pay')->name('pay');

    Route::get('getpaln/{planid}', 'RazorpayController@getPlanbyid')->name('getplan');

    // route for make payment request using post method
    Route::post('dopayment', 'RazorpayController@dopayment')->name('dopayment');
    Route::post('razorpayment', 'RazorpayController@razorPaySuccess')->name('razorpayment');

});

Route::group(['domain' => '{account}.' . \Config::get('app.base_url')], function () {

    /*
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/account', function ($account) {
    return $account;
    });
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'Subdomain\DashboardController@index']);
     */
    Route::get('/', ['as' => 'vcard', 'uses' => 'Subdomain\DashboardController@index', 'middleware' => 'sub_domain_shift']);
    Route::get('/savecard', ['as' => 'makecard', 'uses' => 'Subdomain\DashboardController@CreateVcard', 'middleware' => 'sub_domain_shift']);
    Route::get('/qrcode', ['as' => 'qrcode', 'uses' => 'Subdomain\DashboardController@CreateQrcode', 'middleware' => 'sub_domain_shift']);
});

// Route::get('crop-image', 'ImageCropController@index');
// Route::post('crop-image', ['as' => 'croppie.upload-image', 'uses' => 'ImageCropController@imageCrop']);

// Route::get('image_crop', 'ImageCropController@index1');
// Route::post('image_crop/upload', 'ImageCropController@upload')->name('image_crop.upload');

// Route::get('imagecrop', 'ImageCropController@index2');
// Route::post('imagecrop/upload', 'ImageCropController@upload')->name('image_crop.upload');

// Route::get('step1', ['as' => 'personal_info.step1', 'uses' => 'PersonalInfoController@createStep1']);
// Route::post('step1', ['as' => 'personal_info.step1', 'uses' => 'PersonalInfoController@postCreateStep1']);

// Route::get('step2', 'PersonalInfoController@createStep2');
// Route::post('step2', 'PersonalInfoController@postCreateStep2');
// Route::post('remove-image', 'PersonalInfoController@removeImage');

// Route::get('step3', 'PersonalInfoController@createStep3');
// Route::post('store', 'PersonalInfoController@store');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
