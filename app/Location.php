<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_location';

    // protected $guarded = [];
    protected $fillable = [
        'lname', 'ladd', 'lmobile', 'lmail',
    ];
}
