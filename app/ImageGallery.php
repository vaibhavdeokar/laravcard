<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageGallery extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_gallery';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'svalue',
    ];
}
