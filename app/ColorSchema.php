<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColorSchema extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_color';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'svalue', 'is_active',
    ];
}
