<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_testimonial';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'sduration', 'sdesp',
    ];
}
