<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $this->getdb();

        $payments = Payment::latest()->paginate(5);

        return view('payment.index', compact('payments'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('payment.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        Payment::create($request->all());

        return redirect()->route('payment.index')
            ->with('success', 'Payment created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $payment = Payment::find($id);
        return view('payment.show', compact('payment'));
    }

    public function edit($id)
    {
        $this->getdb();
        $payment = Payment::find($id);
        return view('payment.edit', compact('payment'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $payment = Payment::find($id);

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        $payment->update($request->all());

        return redirect()->route('payment.index')
            ->with('success', 'Payment updated successfully');
    }

    public function destroy($id)
    {
        $this->getdb();
        $payment = Payment::findOrFail($id);
        $payment->delete();

        return redirect()->route('payment.index')
            ->with('success', 'Payment deleted successfully');
    }
}
