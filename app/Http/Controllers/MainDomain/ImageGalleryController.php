<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\ImageGallery;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class ImageGalleryController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth', 'is_mobile_verified']);

    }

    public function index()
    {

        $this->getdb();

        $images = ImageGallery::get();
        // dd($images);
        return view('gallery.index', compact('images'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getdb();
        $userid = \Auth::user()->id;

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imgname = time() . '.' . $request->image->getClientOriginalExtension();
        $folderpath = 'storage/userdata/' . $userid;

        if (!\File::exists($folderpath)) {
            \File::makeDirectory($folderpath, 0755, true, true);
        }

        $request->image->move(public_path($folderpath), $imgname);
        $input['svalue'] = $userid . '/' . $imgname;
        $input['sname'] = $request->title;
        ImageGallery::create($input);

        return back()
            ->with('success', 'Image Uploaded successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getdb();

        // $image = ImageGallery::find($id);

        // \Storage::delete('userdata/' . $image->svalue);
        //unlink(storage_path('app/public/userdata/' . $image->svalue));

        ImageGallery::find($id)->delete();

        return back()
            ->with('success', 'Image removed successfully.');
    }
}
