<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Location;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    use GetSubDomain;

    public function index()
    {
        $this->getdb();

        $location = Location::latest()->paginate(5);

        return view('location.index', compact('location'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('location.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'lname' => 'required',
            'ladd' => 'required',
        ]);

        Location::create($request->all());

        return redirect()->route('location.index')
            ->with('success', 'Location created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getdb();
        $location = Location::find($id);
        return view('location.show', compact('location'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getdb();
        $location = Location::find($id);
        return view('location.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getdb();
        $location = Location::find($id);

        $request->validate([
            'lname' => 'required',
            'ladd' => 'required',
        ]);

        $location->update($request->all());

        return redirect()->route('location.index')
            ->with('success', 'Location updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->getdb();
        $location = Location::findOrFail($id);
        $location->delete();

        return redirect()->route('location.index')
            ->with('success', 'Location deleted successfully');
    }
}
