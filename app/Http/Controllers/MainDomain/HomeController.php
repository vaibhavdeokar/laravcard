<?php

namespace App\Http\Controllers\MainDomain;

use App\Home\MSG91;
use App\Http\Controllers\Controller;
use App\Traits\LoginTrait;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Redirect;

class HomeController extends Controller
{
    use AuthenticatesUsers;

    use LoginTrait;

    // use EncryptionTrait;

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $user = Auth::user();
        $data['subdomain_url'] = 'http://' . $user->subdomain . '.' . \Config::get('app.base_url');
        $data['referral_url'] = route('register', ['ref' => $user->referral_token]);
        $data['referral_count'] = User::where('referrer_id', '=', $user->id)->count();
        //dd($data, $user->id);
        if ($user->mobile_verified == 1) {
            session()->put('status', '');
            return view('maindomain.home', compact('data'));
        } else {
            return redirect()->route('send.otp');
        }

    }

    public function OTPSend($mobileno, $otp)
    {
        $MSG91 = new MSG91();

        //$msg91Response = $MSG91->sendSMS($otp, $mobileno); //$users['mobile']);
        $msg91Response = $MSG91->sendotp($otp, $mobileno); //$users['mobile']);

        // return $msg91Response;
        if ($msg91Response['error']) {
            $response['error'] = 1;
            $response['message'] = $msg91Response['message'];
            $response['loggedIn'] = 1;
        } else {
            $response['error'] = 0;
            $response['message'] = 'Your OTP is created.';
            $response['OTP'] = $otp;
            $response['loggedIn'] = 1;
        }
        // }
        echo json_encode($response);
    }

    public function sendOtp()
    {
        $user = Auth::user();
        $otp = rand(100000, 999999);
        $user->update(['mobile_otp' => $otp]);
        $this->OTPSend($user->mobile, $otp);
        session()->put('status', 'OTP Sent successfully');
        return view('maindomain.mobile', compact('user'));
    }
    public function verifyOtp(Request $request)
    {
        $user = Auth::user();
        $enteredOtp = $request->input('otp');
        $OTP = $user->mobile_otp;

        if ($OTP == $enteredOtp) {
            $user->update(['mobile_verified' => 1, 'mobile_otp' => 0]);
            session()->put('status', 'Mobile verify successfully');
            return redirect()->route('home.maindomain');
        } else {
            session()->put('status', 'Enter otp is not Match');
            return view('maindomain.mobile', compact('user'));
        }
    }
    public function dashboard(Request $request)
    {
        $user = Auth::user();
        $mainUrl = 'http://' . $user->subdomain . '.' . \Config::get('app.base_url');
        return Redirect::to($mainUrl);
        // return Redirect::to('http://user.laravcard.com/dashboard')->with([
        //     'username' => 'vaibhav',
        //     'password' => 'vaibhav',
        // ]);
    }
}
