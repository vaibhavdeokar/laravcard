<?php

namespace App\Http\Controllers\MainDomain;

use App\Achievement;
use App\Education;
use App\Experience;
use App\Http\Controllers\Controller;
use App\Product;
use App\Skill;
use App\Testimonial;
use App\Traits\GetSubDomain;
use App\Userinfo as Userinfo;
use Illuminate\Http\Request;

class PreviewController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth', 'is_mobile_verified']);

    }

    public function index()
    {
        $plan_free = [11, 12, 13, 14, 15];
        return view('preview.index', compact('plan_free'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getdb();
        $user = \Auth::user();
        //dd($user);
        $userinfo = new Userinfo();
        $data = $userinfo->find(1);
        $skills = Skill::all();
        $achievements = Achievement::all();
        $testimonies = Testimonial::all();
        $experiens = Experience::all();
        $educations = Education::all();
        $products = Product::all();
        $vcardPath = asset('vcard/' . $id . '/');
        return view('vcards.' . $id, compact('vcardPath', 'data', 'products', 'skills', 'achievements', 'testimonies', 'experiens', 'educations'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
