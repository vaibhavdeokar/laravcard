<?php

namespace App\Http\Controllers\MainDomain;

use App\ColorSchema;
use App\Http\Controllers\Controller;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);

    }

    public function index()
    {

        $this->getdb();
        // dd(\DB::connection('subdb')->getDatabaseName());
        $colors = ColorSchema::latest()->paginate(5);
        // dd($colors);
        return view('colors.index', compact('colors'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('color.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->getdb();
        $skill = ColorSchema::find($id);
        return view('colors.edit', compact('skill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->getdb();
        $skill = ColorSchema::find($id);

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        $skill->update($request->all());

        return redirect()->route('colorschema.index')
            ->with('success', 'Color Change successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
