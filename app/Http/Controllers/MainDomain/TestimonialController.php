<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Testimonial;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $this->getdb();

        $testimonials = Testimonial::latest()->paginate(5);

        return view('testimonial.index', compact('testimonials'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('testimonial.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        Testimonial::create($request->all());

        return redirect()->route('testimonial.index')
            ->with('success', 'Testimonial created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $testimonial = Testimonial::find($id);
        return view('testimonial.show', compact('testimonial'));
    }

    public function edit($id)
    {
        $this->getdb();
        $testimonial = Testimonial::find($id);
        return view('testimonial.edit', compact('testimonial'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $testimonial = Testimonial::find($id);

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        $testimonial->update($request->all());

        return redirect()->route('testimonial.index')
            ->with('success', 'Testimonial updated successfully');

    }

    public function destroy($id)
    {
        $this->getdb();
        $testimonial = Testimonial::findOrFail($id);
        $testimonial->delete();

        return redirect()->route('testimonial.index')
            ->with('success', 'Testimonial deleted successfully');
    }
}
