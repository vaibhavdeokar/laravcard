<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Social;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class SocialController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $this->getdb();

        $socials = Social::latest()->paginate(5);

        return view('social.index', compact('socials'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('social.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        Social::create($request->all());

        return redirect()->route('social.index')
            ->with('success', 'Social created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $social = Social::find($id);
        return view('social.show', compact('social'));
    }

    public function edit($id)
    {
        $this->getdb();
        $social = Social::find($id);
        return view('social.edit', compact('social'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $social = Social::find($id);

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        $social->update($request->all());

        return redirect()->route('social.index')
            ->with('success', 'Social updated successfully');
    }

    public function destroy($id)
    {
        $this->getdb();
        $social = Social::findOrFail($id);
        $social->delete();

        return redirect()->route('social.index')
            ->with('success', 'Social deleted successfully');
    }
}
