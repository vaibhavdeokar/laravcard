<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subscription.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->sub_name == 'unsubscribe') {
            return $this->unsubscribe();
        } else if ($request->sub_name == 'renew') {
            return $this->Renew();
        } else {
            return redirect()->route('plan.index')
                ->with('success', 'Unsubscribe Successfully');
        }
        // return redirect()->route('pay_payment', $request);
    }

    public function unsubscribe()
    {
        // dd($request);
        $user = \Auth::user();
        // $user->subscription($request->slug)->cancel(true);
        $plan = app('rinvex.subscriptions.plan')->find(1);
        $subscription = app('rinvex.subscriptions.plan_subscription')->where('user_id', $user->id)->first();

        // dd($plan, $subscription);
        // Change subscription plan
        $subscription->changePlan($plan);
        //dd('done');
        return response()->json(['status' => true, 'message' => 'successful']);

    }

    public function Renew()
    {
        return redirect()->route('plan.index')
            ->with('success', 'Unsubscribe Successfully');
        // dd($request);
        // return $request;
        // return redirect()->action('MainDomain\PayumoneyController@payment', $request);

        // $user = \Auth::user();
        // $user->subscription($request->slug)->renew();
        return redirect()->route('plan.index')
            ->with('success', 'Plan Renew Successfully');
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
