<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rinvex\Subscriptions\Models\Plan;
use Rinvex\Subscriptions\Models\PlanFeature;
use Rinvex\Subscriptions\Models\PlanSubscription;
use Rinvex\Subscriptions\Models\PlanSubscriptionUsage;

class PlanController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $user = \Auth::user();
        $plans = Plan::all();
        // dd($plans);
        $psub = new PlanSubscription();
        $data = $psub->where('user_id', $user->id)->first();
        $data['is_active'] = $user->subscription($data->slug)->active();
        $data['is_ended'] = $user->subscription($data->slug)->ended();
        return view('plan.index', compact('plans', 'data'));

    }

    public function CustomSubscribe()
    {
        $plan = new Plan();
        $paln1 = new PlanFeature();
        $paln2 = new PlanSubscription();
        $paln3 = new PlanSubscriptionUsage();

        /*$plan = app('rinvex.subscriptions.plan')->create([
        'name' => 'Free',
        'description' => 'Free plan',
        'price' => 0.00,
        'signup_fee' => 0.00,
        'invoice_period' => 0,
        'invoice_interval' => 'month',
        'trial_period' => 0,
        'trial_interval' => 'day',
        'sort_order' => 1,
        'currency' => 'INR',
        ]);

        $plan = app('rinvex.subscriptions.plan')->create([
        'name' => 'Premium',
        'description' => 'Premium plan',
        'price' => 500.00,
        'signup_fee' => 200.00,
        'invoice_period' => 12,
        'invoice_interval' => 'month',
        'trial_period' => 0,
        'trial_interval' => 'day',
        'sort_order' => 2,
        'currency' => 'INR',
        ]);

        $plan = app('rinvex.subscriptions.plan')->create([
        'name' => 'Gold',
        'description' => 'Gold plan',
        'price' => 1000.00,
        'signup_fee' => 400.00,
        'invoice_period' => 12,
        'invoice_interval' => 'month',
        'trial_period' => 0,
        'trial_interval' => 'day',
        'sort_order' => 3,
        'currency' => 'INR',
        ]);
         */
        // Create multiple plan features at once
        // $plan->features()->saveMany([
        //     new PlanFeature(['name' => 'listings', 'value' => 50, 'sort_order' => 1]),
        //     new PlanFeature(['name' => 'pictures_per_listing', 'value' => 10, 'sort_order' => 5]),
        //     new PlanFeature(['name' => 'listing_duration_days', 'value' => 30, 'sort_order' => 10, 'resettable_period' => 1, 'resettable_interval' => 'month']),
        //     new PlanFeature(['name' => 'listing_title_bold', 'value' => 'Y', 'sort_order' => 15]),
        // ]);

        // $plan = $plan->find(3);
        // dd($plan); // get plan info
        // Check if the plan is free
        // dd($plan->isFree());

        // Check if the plan has trial period
        // dd($plan->hasTrial());

        // Check if the plan has grace period
        // dd($plan->hasGrace());

        //user subscribe the plan
        // $user = \App\User::find(17);
        // $plan = app('rinvex.subscriptions.plan')->find(1);

        // $user->newSubscription('free', $plan);

        // Get bookings of the given user
        // $user = \App\User::find(17);
        // $bookingsOfUser = app('rinvex.subscriptions.plan_subscription')->ofUser($user)->get();

        // Get subscriptions by plan
        // $subscriptions = app('rinvex.subscriptions.plan_subscription')->byPlanId(3)->get();
        // $subscriptions = app('rinvex.subscriptions.plan_subscription')->findEndingTrial(3)->get();

        // dd($subscriptions);

        $plan = app('rinvex.subscriptions.plan')->find(2);
        $subscription = app('rinvex.subscriptions.plan_subscription')->where('user_id', 17)->first();

        //dd($subscription);
        // Change subscription plan
        $subscription->changePlan($plan);
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
