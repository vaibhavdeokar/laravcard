<?php

namespace App\Http\Controllers\MainDomain;

use App\Education;
use App\Http\Controllers\Controller;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $this->getdb();

        $educations = Education::latest()->paginate(5);

        return view('education.index', compact('educations'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('education.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        Education::create($request->all());

        return redirect()->route('education.index')
            ->with('success', 'Education created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $education = Education::find($id);
        return view('education.show', compact('education'));
    }

    public function edit($id)
    {
        $this->getdb();
        $education = Education::find($id);
        return view('education.edit', compact('education'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $education = Education::find($id);

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        $education->update($request->all());

        return redirect()->route('education.index')
            ->with('success', 'Education updated successfully');

    }

    public function destroy($id)
    {
        $this->getdb();
        $education = Education::findOrFail($id);
        $education->delete();

        return redirect()->route('education.index')
            ->with('success', 'Education deleted successfully');
    }
}
