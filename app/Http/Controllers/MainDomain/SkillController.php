<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Skill;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth', 'is_mobile_verified', 'Free_plan']);

    }

    public function index()
    {
        $this->getdb();
        // dd(\DB::connection('subdb')->getDatabaseName());
        $skills = Skill::latest()->paginate(5);

        return view('skills.index', compact('skills'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('skills.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        Skill::create($request->all());

        return redirect()->route('skills.index')
            ->with('success', 'Skill created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $skill = Skill::find($id);
        return view('skills.show', compact('skill'));
    }

    public function edit($id)
    {
        $this->getdb();
        $skill = Skill::find($id);
        return view('skills.edit', compact('skill'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $skill = Skill::find($id);

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        $skill->update($request->all());

        return redirect()->route('skills.index')
            ->with('success', 'Skill updated successfully');
    }

    public function destroy($id)
    {
        $this->getdb();
        $skill = Skill::findOrFail($id);
        $skill->delete();

        return redirect()->route('skills.index')
            ->with('success', 'Skill deleted successfully');
    }
}
