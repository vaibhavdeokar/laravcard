<?php

namespace App\Http\Controllers\MainDomain;

use App\Experience;
use App\Http\Controllers\Controller;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class ExperienceController extends Controller
{
    use GetSubDomain;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index()
    {
        $this->getdb();

        $experiences = Experience::latest()->paginate(5);

        return view('experience.index', compact('experiences'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('experience.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        Experience::create($request->all());

        return redirect()->route('experience.index')
            ->with('success', 'Experience created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $experience = Experience::find($id);
        return view('experience.show', compact('experience'));
    }

    public function edit($id)
    {
        $this->getdb();
        $experience = Experience::find($id);
        return view('experience.edit', compact('experience'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $experience = Experience::find($id);

        $request->validate([
            'sname' => 'required',
            'sduration' => 'required',
            'sdesp' => 'required',
        ]);

        $experience->update($request->all());

        return redirect()->route('experience.index')
            ->with('success', 'Experience updated successfully');

    }

    public function destroy($id)
    {
        $this->getdb();
        $experience = Experience::findOrFail($id);
        $experience->delete();

        return redirect()->route('experience.index')
            ->with('success', 'Experience deleted successfully');
    }
}
