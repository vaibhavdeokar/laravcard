<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Tzsk\Payu\Facade\Payment;

class PayumoneyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function payment(Request $request)
    {
        //dd($request);

        $user = \Auth::user();

        $attributes = [
            'txnid' => strtoupper(str_random(8)), # Transaction ID.
            'amount' => $request->amount, //rand(100, 999), # Amount to be charged.
            'productinfo' => $request->slug,
            'firstname' => $request->id, //"John", # Payee Name.
            'lastname' => $request->id, //"John", # Payee Name.
            'email' => $user->email, //"john@doe.com", # Payee Email Address.
            'phone' => $user->mobile, //"9876543210", # Payee Phone Number
        ];

        return Payment::make($attributes, function ($then) {
            #$then->redirectTo('payment/status');
            # OR...
            $then->redirectRoute('pay_payment.status');
            # OR...
            #$then->redirectAction('PaymentController@status');
        });
    }

    public function status()
    {
        $payment = Payment::capture();

        if ($payment->isCaptured()) {
            $payment->save();
            // dd($payment);
            $plan = app('rinvex.subscriptions.plan')->find($payment->firstname);
            $subscription = app('rinvex.subscriptions.plan_subscription')->where('user_id', \Auth::user()->id)->first();

            // Change subscription plan
            $subscription->changePlan($plan);

            return redirect()->route('plan.index')
                ->with('success', 'Payment Done successfully');
        } else {
            return redirect()->route('plan.index')
                ->with('error', 'Something Went Wrong');
        }
    }
}
