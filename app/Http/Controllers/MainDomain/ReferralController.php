<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Referral;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    use GetSubDomain;

    public function index()
    {
        $this->getdb();
        $referrals = [];
        $data = Referral::latest()->paginate(5);
        foreach ($data as $r) {
            $user = \App\User::where('mobile', '=', $r->svalue)->exists();
            // dd($user);
            if ($user) {
                $u = \App\User::where('mobile', '=', $r->svalue)->first();
                // dd($u->subscribedTo([1, 2, 3]));
                $r->is_register = true;
                $r->is_subscrib = ($u->subscribedTo(1) || $u->subscribedTo(2) || $u->subscribedTo(3)) ? true : false;
                $r->by_whom = ($u->referrer_id == \Auth::user()->id) ? 'By You' : 'By Other User';
                // dd($u->subscribedTo(1));
            } else {
                $r->is_register = false;
                $r->is_subscrib = false;
                $r->by_whom = 'Not Yet';
            }
            //dd($user->subscribedTo(1));
            //$r->is_register = (\App\User::where('mobile', '=', $r->svalue)->exists()) ? true : false;
            // if ($r->register == true) {
            //     $user = \App\User::where('mobile', '=', $r->svalue)->get();
            //     $r->is_subscrib = $user->subscribedTo(1);
            //     //    $r->is_subscrib = (exists() == true) ? \App\User::where('mobile', '=', $r->svalue)->subscribedTo(1) : 'No Data';
            // } else {
            //     $r->is_subscrib = false;
            // }

            array_push($referrals, $r);
        }
        // dd($referrals);
        // $user = User::where('mobile',$referral->svalue)
        return view('referral.index', compact('referrals'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('referral.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        Referral::create($request->all());

        return redirect()->route('referral.index')
            ->with('success', 'Referral created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->getdb();
        $referral = Referral::find($id);
        dd($referral);
        return view('product.show', compact('referral'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function edit(Referral $referral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Referral $referral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Referral  $referral
     * @return \Illuminate\Http\Response
     */
    public function destroy(Referral $referral)
    {
        //
    }
}
