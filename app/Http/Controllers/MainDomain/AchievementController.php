<?php

namespace App\Http\Controllers\MainDomain;

use App\Achievement;
use App\Http\Controllers\Controller;
use App\Traits\GetSubDomain;
use Illuminate\Http\Request;

class AchievementController extends Controller
{
    use GetSubDomain;

    public function index()
    {
        $this->getdb();

        $achievements = Achievement::latest()->paginate(5);

        return view('achievements.index', compact('achievements'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function create()
    {
        return view('achievements.create');
    }

    public function store(Request $request)
    {
        $this->getdb();

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        Achievement::create($request->all());

        return redirect()->route('achievements.index')
            ->with('success', 'Achievement created successfully.');
    }

    public function show($id)
    {
        $this->getdb();
        $achievement = Achievement::find($id);
        return view('achievements.show', compact('achievement'));
    }

    public function edit($id)
    {
        $this->getdb();
        $achievement = Achievement::find($id);
        return view('achievements.edit', compact('achievement'));
    }

    public function update(Request $request, $id)
    {
        $this->getdb();
        $achievement = Achievement::find($id);

        $request->validate([
            'sname' => 'required',
            'svalue' => 'required',
        ]);

        $achievement->update($request->all());

        return redirect()->route('achievements.index')
            ->with('success', 'Achievement updated successfully');
    }

    public function destroy($id)
    {
        $this->getdb();
        $achievement = Achievement::findOrFail($id);
        $achievement->delete();

        return redirect()->route('achievements.index')
            ->with('success', 'Achievement deleted successfully');
    }
}
