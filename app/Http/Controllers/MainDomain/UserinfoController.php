<?php

namespace App\Http\Controllers\MainDomain;

use App\Http\Controllers\Controller;
use App\Traits\GetSubDomain;
use App\Userinfo;
use Auth;
use Hash;
use Illuminate\Http\Request;
use JeroenDesloovere\VCard\VCard;

class UserinfoController extends Controller
{
    use GetSubDomain;

    private $folderpath;

    public function __construct()
    {
        $this->middleware(['auth']);

    }

    public function index(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->get();
        // $personinfo = $request->session()->get('personinfo');
        // dd($pinfo[0]->ckey);
        return view('maindomain.userinfo', compact('pinfo'));
    }
    public function index1(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->find(1);
        // $this->CreateVcard($pinfo);
        return view('maindomain.userinfo1', compact('pinfo'));
    }

    public function index2(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->find(1); //-get();
        return view('maindomain.userinfo2', compact('pinfo'));
    }

    public function index3(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->find(1); //-get();
        return view('maindomain.userinfo3', compact('pinfo'));
    }

    public function companylogo(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->find(1); //-get();
        return view('maindomain.companylogo', compact('pinfo'));
    }

    public function profilelogo(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $pinfo = $userinfo->find(1); //-get();
        return view('maindomain.profilelogo', compact('pinfo'));
    }

    public function passwordreset(Request $request)
    {
        return view('maindomain.password_reset');
    }

    public function storePersonalinfo(Request $request)
    {
        $this->getdb();

        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $data = $request->all();
        // dd($data);
        $finalArray = array();
        foreach ($data as $key => $value) {
            if ($key != '_token') {
                $arr['cname'] = 'personal_info';
                $arr['ckey'] = $key;
                $arr['cvalue'] = $value;
                array_push($finalArray, $arr);
                Userinfo::updateOrCreate(['ckey' => $key], ['cname' => 'personal_info', 'ckey' => $key, 'cvalue' => $value]);
            }
            //if ($key != '_token') {
            // $userinfo->cname = 'personal_info';
            // $userinfo->ckey = $key;
            // $userinfo->cvalue = $value;
            // $userinfo->save();
            //}
        }
        // $userinfo->save();
        // Userinfo::insert($finalArray); // for insert only
        // Userinfo::updateOrCreate($finalArray);
        // dd($finalArray);
        return redirect()->route('home.maindomain')
            ->with('success', 'Company Info Updated successfully.');
    }

    public function storeinfo1(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile' => 'required',
            'profession' => 'required',
            'website' => 'required',
            'about_me' => 'required',
        ]);

        Userinfo::where('id', 1)->update($validatedData);

        return redirect()->route('userinfo1')
            ->with('success', 'Company Info Updated successfully.');
    }

    public function storeinfo2(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $validatedData = $request->validate([
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'skype_id' => 'required',
            'youtube_about' => 'required',
            'fb_msg' => 'required',
        ]);

        Userinfo::where('id', 1)->update($validatedData);

        return redirect()->route('userinfo2')
            ->with('success', 'Company Info Updated successfully.');
    }

    public function storeinfo3(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $validatedData = $request->validate([
            'cname' => 'required',
            'ctagline' => 'required',
            'youtube_company' => 'required',
            'cabout' => 'required',
            'cmap' => 'required',
        ]);

        Userinfo::where('id', 1)->update($validatedData);

        return redirect()->route('userinfo3')
            ->with('success', 'Company Info Updated successfully.');
    }

    public function storecompanylogo(Request $request)
    {
        // dd($request);
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $validatedData = $request->validate([
            'company_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($files = $request->file('company_logo')) {
            // $destinationPath = $this->folderpath; // upload path
            $profileImage = "Comapanylogo-" . date('YmdHis') . "." . $files->getClientOriginalExtension();
            $folderpath = 'storage/userdata/' . Auth::user()->id;
            $this->checkFile(public_path($folderpath));
            $files->move($folderpath, $profileImage);
            $insert['company_logo'] = Auth::user()->id . "/$profileImage";
        }

        Userinfo::where('id', 1)->update($insert);

        return redirect()->route('companylogo')
            ->with('success', 'Company Info Updated successfully.');
    }

    public function storeprofilelogo(Request $request)
    {
        $this->getdb();
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        if ($request->ajax()) {
            $image_data = $request->image;
            $image_array_1 = explode(";", $image_data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'profileimg-' . date('YmdHis') . '.png';
            $folderpath = 'storage/userdata/' . Auth::user()->id;
            $this->checkFile(public_path($folderpath));
            $upload_path = public_path($folderpath) . '/' . $image_name;
            file_put_contents($upload_path, $data);
            $insert['profile_logo'] = Auth::user()->id . "/$image_name";
            Userinfo::where('id', 1)->update($insert);
            return response()->json(['path' => $folderpath . '/' . $image_name]);
        }
    }

    public function checkFile($path)
    {
        if (!\File::exists($path)) {
            \File::makeDirectory($path, 0755, true, true);
        }
    }

    public function storepassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required'],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            // The passwords not matches
            return redirect()->back()->withErrors("Your current password does not matches with the password you provided. Please try again.");
            //return response()->json(['errors' => ['current' => ['Current password does not match']]], 422);
        }
        //uncomment this if you need to validate that the new password is same as old one

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->withErrors("New Password cannot be same as your current password. Please choose a different password.");
            // return response()->json(['errors' => ['current' => ['New Password cannot be same as your current password']]], 422);
        }

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('new_password'));
        $user->save();
        return redirect()->route('home.maindomain')
            ->with('status', 'Password Change successfully.');
    }

    public function CreateVcard($user)
    {
        // define vcard
        $vcard = new VCard();

        // define variables
        $lastname = $user->first_name;
        $firstname = $user->last_name;
        $additional = '';
        $prefix = '';
        $suffix = '';

        // add personal data
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);

        // add work data
        $vcard->addCompany($user->cname);
        $vcard->addJobtitle($user->profession);
        // $vcard->addRole('Data Protection Officer');
        $vcard->addEmail($user->email);
        $vcard->addPhoneNumber($user->mobile, 'PREF;WORK');
        $vcard->addPhoneNumber($user->phone, 'WORK');
        $vcard->addAddress(null, null, $user->address, null, null, null, null);
        $vcard->addLabel('Address');
        $vcard->addURL($user->website);

        $vcard->addPhoto(public_path('storage/userdata/' . $user->profile_logo));

        // return vcard as a string
        //return $vcard->getOutput();

        // return vcard as a download
        return $vcard->download();

        // save vcard on disk
        //$vcard->setSavePath('/path/to/directory');
        //$vcard->save();
    }
}
