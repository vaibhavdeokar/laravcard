<?php

namespace App\Http\Controllers\SubDomain;

use App\Achievement;
use App\Education;
use App\Experience;
use App\Http\Controllers\Controller;
use App\Product;
use App\Skill;
use App\Testimonial;
use App\Userinfo as Userinfo;
use Illuminate\Support\Facades\Schema;
use JeroenDesloovere\VCard\VCard;
use Session;

class DashboardController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        // $data = new createData();
        // dd($data->dataCreate('easycukv_demo'));
        // $fi = \Storage::disk("resources_views")->get("vcards/11/assets/style.css");

        // $files = \Storage::allFiles("public");
        // dd($files);
        $user = Session::get('userInfo');
        // dd(asset('storage/vcards/11/index.blade.php'));

        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');

        $tblchk = Schema::connection("subdb")->hasTable($userinfo->getTable());

        if ($user['mobile_verified'] == 1 && $tblchk) {
            $data = $userinfo->find(1);
            $skills = Skill::all();
            $achievements = Achievement::all();
            $testimonies = Testimonial::all();
            $experiens = Experience::all();
            $educations = Education::all();
            $products = Product::all();
            // connectify('success', 'Connection Found', 'Success Message Here');
            // notify()->success('Welcome to Laravel Notify ⚡️', 'My custom title');
            // drakify('success');
            $vcardPath = asset('vcard/13/');
            // return view('vcards.' . $user['vcard'] . '.index', compact('data', 'products', 'skills', 'achievements', 'testimonies', 'experiens', 'educations'));
            return view('vcards.13', compact('vcardPath', 'data', 'products', 'skills', 'achievements', 'testimonies', 'experiens', 'educations'));

        } else {
            return response()->json([
                'error' => 'This account is not activated.',
            ], 401);
        }
    }

    public function CreateVcard()
    {
        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $user = $userinfo->find(1);
        // define vcard
        $vcard = new VCard();

        // define variables
        $lastname = $user->first_name;
        $firstname = $user->last_name;
        $additional = '';
        $prefix = '';
        $suffix = '';

        // add personal data
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);

        // add work data
        $vcard->addCompany($user->cname);
        $vcard->addJobtitle($user->profession);
        // $vcard->addRole('Data Protection Officer');
        $vcard->addEmail($user->email);
        $vcard->addPhoneNumber($user->mobile, 'PREF;WORK');
        $vcard->addPhoneNumber($user->phone, 'WORK');
        $vcard->addAddress(null, null, $user->address, null, null, null, null);
        $vcard->addLabel('Address');
        $vcard->addURL($user->website);

        $vcard->addPhoto(public_path('storage/userdata/' . $user->profile_logo));

        // return vcard as a string
        //return $vcard->getOutput();

        // return vcard as a download
        $vcard->download();

        return $this->index();
        // save vcard on disk
        //$vcard->setSavePath('/path/to/directory');
        //$vcard->save();
    }

    public function CreateQrcode()
    {
        $url = url()->current();
        return view('qrcode', compact('url'));
    }
}
