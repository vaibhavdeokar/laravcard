<?php

namespace App\Http\Controllers\Home;

use App\Home\MSG91;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Session;

class SubdomainCheckController extends Controller
{
    public function check(Request $request)
    {
        if ($request->get('subdomain')) {
            $subdomain = $request->get('subdomain');
            $data = User::where('subdomain', $subdomain)->count();
            if ($data > 0) {
                echo 'not_unique';
            } else {
                echo 'unique';
            }
        }

        if ($request->get('mobile')) {
            $mobile = $request->get('mobile');
            $data = User::where('mobile', $mobile)->count();
            if ($data > 0) {
                echo 'not_unique';
            } else {
                echo 'unique';
            }
        }
    }

    public function sendOtp(Request $request)
    {

        $response = array();
        $mobileno = $request->get('mobile');

        $otp = rand(100000, 999999);
        $MSG91 = new MSG91();

        //$msg91Response = $MSG91->sendSMS($otp, $mobileno); //$users['mobile']);
        $msg91Response = $MSG91->sendotp($otp, $mobileno); //$users['mobile']);

        //return $msg91Response;

        if ($msg91Response['error']) {
            $response['error'] = 1;
            $response['message'] = $msg91Response['message'];
            $response['loggedIn'] = 1;
        } else {
            //Removing Session variable
            Session::forget('is_mobile_verified');
            Session::put('OTP', $otp);
            Session::put('mobileno', $mobileno);
            $response['error'] = 0;
            $response['message'] = 'Your OTP is created.';
            $response['OTP'] = $otp;
            $response['loggedIn'] = 1;
        }
        // }
        echo json_encode($response);
    }

    public function verifyOtp(Request $request)
    {

        $response = array();

        $enteredOtp = $request->input('otp');

        $OTP = $request->session()->get('OTP');

        if ($OTP == $enteredOtp) {

            // Updating user's status "isVerified" as 1.

            //User::where('id', $userId)->update(['isVerified' => 1]);

            //Removing Session variable
            Session::forget('OTP');

            Session::put('is_mobile_verified', 1);

            $response['error'] = 0;
            $response['isVerified'] = 1;
            $response['loggedIn'] = 1;
            $response['message'] = "Your Number is Verified.";
        } else {
            Session::put('is_mobile_verified', 0);
            $response['error'] = 1;
            $response['isVerified'] = 0;
            $response['loggedIn'] = 1;
            $response['message'] = "OTP does not match.";
        }

        echo json_encode($response);
    }

}
