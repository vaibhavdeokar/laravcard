<?php

namespace App\Http\Controllers;

use App\PersonalInfo;
use Illuminate\Http\Request;

class PersonalInfoController extends Controller
{
    public function createStep1(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');
        return view('forms.step1', compact('personinfo'));
    }

    public function postCreateStep1(Request $request)
    {

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            // 'profession' => 'required',
            // 'email' => 'required',
            // 'mobile' => 'required|numeric',
            // 'whatsapp' => 'required|numeric',
            // 'website' => 'required',
            // 'skype_id' => 'required',
            // 'about_me' => 'required',
            // 'fb_msg_url' => 'required',
            // 'profile_img' => 'required',
            // 'company_logo' => 'required',
        ]);

        if (empty($request->session()->get('personinfo'))) {
            $personinfo = new PersonalInfo();
            $personinfo->fill($validatedData);
            $request->session()->put('personinfo', $personinfo);
        } else {
            $personinfo = $request->session()->get('personinfo');
            $personinfo->fill($validatedData);
            $request->session()->put('personinfo', $personinfo);
        }

        return redirect('step2');

    }

    public function createStep2(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');
        return view('forms.step2', compact('personinfo', $personinfo));
    }

    public function postCreateStep2(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');

        if (!isset($personinfo->company_logo)) {
            $request->validate([
                'profile_img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $fileName = "companylogo-" . time() . '.' . request()->profile_img->getClientOriginalExtension();

            $request->profile_img->move('storage/crop_image', $fileName);
            $personinfo = $request->session()->get('personinfo');

            $personinfo->company_logo = $fileName;

            $request->session()->put('personinfo', $personinfo);

            return $personinfo->company_logo . '-' . $personinfo->profile_img;
        }

        return redirect('step3');

    }

    public function removeImage(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');
        $profileimg = asset('storage/crop_image/' . $personinfo->profile_img);
        $comapanylogo = asset('storage/crop_image/' . $personinfo->company_logo);
        $personinfo->profile_img = null;
        $personinfo->company_logo = null;
        if (file_exists($profileimg)) {
            @unlink($profileimg);
        }
        if (file_exists($comapanylogo)) {
            @unlink($comapanylogo);
        }
        return view('forms.step2', compact('personinfo', $personinfo));
    }

    public function createStep3(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');
        return view('forms.step3', compact('personinfo', $personinfo));
    }

    public function store(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');
        $personinfo->save();
        return redirect('/products');
    }
}
