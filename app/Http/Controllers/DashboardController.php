<?php

namespace App\Http\Controllers;

use App\PersonalInfo as Userinfo;

class DashboardController extends Controller
{
    //add contructor
    public function __construct()
    {
        $this->middleware(['sub_domain_shift', 'sub_auth']);
    }

    public function index()
    {

        $userinfo = new Userinfo();
        $userinfo->setConnection('subdb');
        $data = $userinfo->find(1);
        // $data = \Auth::user();
        // return $data;
        return view('dashboard', compact('data'));
    }
}
