<?php

namespace App\Http\Controllers;

use App\Razor;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use Redirect;
use Session;

class RazorpayController extends Controller
{
    public function payWithRazorpay()
    {
        return view('payWithRazorpay');
    }

    public function payment(Request $request)
    {
        //Input items of form
        $input = $request->all();
        dd($input);
        //get API Configuration
        $api = new Api(config('razor.razor_key'), config('razor.razor_secret'));
        //Fetch payment information by razorpay_payment_id
        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if (count($input) && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount' => $payment['amount']));

            } catch (\Exception $e) {
                return $e->getMessage();
                \Session::put('error', $e->getMessage());
                return redirect()->back();
            }

            // Do something here for store payment details in database...
        }

        \Session::put('success', 'Payment successful, your order will be despatched in the next 48 hours.');
        return redirect()->back();
    }

    public function razorpayProduct()
    {
        return view('payWithRazorpay');
    }

    public function RazorThankYou()
    {
        return view('thankyou');
    }

    public function makeSubscription(Request $request)
    {
        // return $request->all();
        $posturl = "https://" . \Config::get('razor.razor_key') . ":" . \Config::get('razor.razor_secret') . "@api.razorpay.com/v1/subscriptions";
        // return $posturl;
        $isError = false;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $posturl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "plan_id=" . $request->plan_id . "&total_count=12&customer_notify=1",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "ssl_method : SOAP_SSL_METHOD_SSLv3",
                "soap_version: SOAP_1_2",
            ),
        ));

        $response = curl_exec($curl);
        //$err = curl_error($curl);

        if (curl_errno($curl)) {
            $isError = true;
            $errorMessage = curl_error($curl);
        }

        curl_close($curl);

        // return $output;
        \Log::info($response);

        if ($isError) {
            return response()->json(['error' => 1, 'message' => $errorMessage]);
        } else {
            return response()->json(['error' => 0, 'message' => $response]);
        }

    }

    public function getPlanbyid($planid)
    {
        // return $request->all();
        //$posturl = "https://" . \Config::get('razor.razor_key') . ":" . \Config::get('razor.razor_secret') . "@https://api.razorpay.com/v1/plans/$planid";
        // return $posturl;
        $isError = false;
        $geturl = 'https://api.razorpay.com/v1/plans/' . $planid;
        // return $geturl;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $geturl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_USERPWD, \Config::get("razor.razor_key") . ':' . \Config::get("razor.razor_secret"));

        $response = curl_exec($ch);
        //$err = curl_error($ch);

        if (curl_errno($ch)) {
            $isError = true;
            $errorMessage = curl_error($ch);
        }

        curl_close($ch);

        // return $output;
        \Log::info($response);

        if ($isError) {
            return response()->json(['error' => 1, 'message' => $errorMessage]);
        } else {
            return response()->json(['error' => 0, 'message' => $response]);
        }

    }

    public function pay()
    {
        return view('pay');
    }

    public function dopayment(Request $request)
    {
        //Input items of form
        $input = $request->all();

        // Please check browser console.
        print_r($input);
        exit;
    }

    public function razorPaySuccess(Request $request)
    {
        // user subscribe change the plan
        $plan = app('rinvex.subscriptions.plan')->find($request->plan_id);
        $subscription = app('rinvex.subscriptions.plan_subscription')->where('user_id', \Auth::user()->id)->first();
        $subscription->changePlan($plan);

        $data = [
            'userid' => \Auth::user()->id,
            'payid' => $request->payment_id,
            'subid' => $subscription->id,
        ];
        $getId = Razor::insertGetId($data);
        $arr = array('msg' => 'Payment successfully credited', 'status' => true);
        return Response()->json($arr);
    }
}
