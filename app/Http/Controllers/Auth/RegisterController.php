<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\DBcreate;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;
    use DBcreate;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request)
    {
        if ($request->has('ref')) {
            session(['referrer' => $request->query('ref')]);
        }

        return view('auth.register');
    }

    // protected function registered(Request $request, $user)
    // {
    // if ($user->referrer !== null) {
    // Notification::send($user->referrer, newReferrerBonus($user));
    // }
    // dd($request, $user);
    // return redirect()->route('verify',[$request,$user]);
    // return redirect('/verify', [$request, $user]);
    // return redirect($this->redirectPath());
    // }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:maindb.users'],
            'password' => ['required', 'string', 'min:1', 'confirmed'],
            'mobile' => ['required', 'numeric', 'min:11'],
            'subdomain' => ['required', 'string', 'max:100'],
            // 'mobile' => [function ($attribute, $value, $fail) {
            //     if (Session::get('is_mobile_verified') != 1) {
            //         $fail(':attribute Needs to Verified First');
            //     }
            // }],
        ]);
        /*
    $rules = array(
    'name' => ['required', 'string', 'max:255'],
    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    'password' => ['required', 'string', 'min:1', 'confirmed'],
    'mobile' => [function ($attribute, $value, $fail) {
    if (Session::get('is_mobile_verified') != 1) {
    $fail(':attribute Needs to Verified First');
    }
    }],
    'subdomain' => ['required', 'string', 'max:100', 'unique:users'],
    );
    $messages = array(
    'name.required' => 'Name is Required',
    'email.required' => 'Email is Required',
    'subdomain.unique' => 'Subdomain is not Unique.',
    );
    return Validator::make($data, $rules, $messages);
     */
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $referrer = User::where('referral_token', session()->pull('referrer'))->first();

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'subdomain' => $data['subdomain'],
            'subdb' => $DbName,
            'password' => Hash::make($data['password']),
            'referrer_id' => $referrer ? $referrer->id : null,
            'referral_token' => $referral_code,
            'referral_code' => $referral_code,
        ]);

        $plan = app('rinvex.subscriptions.plan')->find(1);
        $user->newSubscription($user->id, $plan);

        return $user;

        $DbName = 'easycukv_' . $user->subdomain;

        $this->CreateDB($DbName, $user->subdomain);

        $this->createPersonalTable();

        $this->FillPersonalInfo($user->name, $user->email);

        $this->TwoColumnTable('tbl_achievement');
        $this->TwoColumnTable('tbl_payment');
        $this->TwoColumnTable('tbl_product');
        $this->TwoColumnTable('tbl_social');
        $this->TwoColumnTable('tbl_skills');
        $this->TwoColumnTable('tbl_color');
        $this->TwoColumnTable('tbl_gallery');
        //
        $this->ThreeColumnTable('tbl_testimonial');
        $this->ThreeColumnTable('tbl_education');
        $this->ThreeColumnTable('tbl_experience');

        $this->FillColorTbl();

        $referral_code = substr(md5(rand(0, 9) . $data['email'] . time()), 0, 32);

        $plan = app('rinvex.subscriptions.plan')->find(1);
        $user->newSubscription($user->id, $plan);

        return $user;
    }

}
