<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageCropController extends Controller
{
    public function index()
    {
        return view('image.crop_image');
    }

    public function imageCrop(Request $request)
    {
        $image_file = $request->image;
        list($type, $image_file) = explode(';', $image_file);
        list(, $image_file) = explode(',', $image_file);
        $image_file = base64_decode($image_file);
        $image_name = time() . '_' . rand(100, 999) . '.png';
        $path = public_path('uploads/' . $image_name);
        file_put_contents($path, $image_file);
        return response()->json(['status' => true]);
    }

    public function index1()
    {
        return view('image.crop-image');
    }

    public function upload(Request $request)
    {
        $personinfo = $request->session()->get('personinfo');

        if ($request->ajax()) {
            $image_data = $request->image;
            $image_array_1 = explode(";", $image_data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'profileimg-' . time() . '.png';
            $upload_path = public_path('storage/crop_image/' . $image_name);
            file_put_contents($upload_path, $data);
            $personinfo = $request->session()->get('personinfo');
            $personinfo->profile_img = $image_name;
            $request->session()->put('personinfo', $personinfo);
            return response()->json(['path' => '/storage/crop_image/' . $image_name]);
        }
    }

    public function index2()
    {
        return view('image.imagecrop');
    }

}
