<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class GoldPlanAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // dd(Auth::user()->isAdmin());
            if (Auth::user()->isFree() || Auth::user()->isPremium()) {
                return redirect()->route('home.maindomain');
            } //allow admin to proceed with request
            else if (Auth::user()->isGold()) {
                return $next($request);
            }
        }

        abort(404);
    }
}
