<?php

namespace App\Http\Middleware;

use Closure;

class PlanAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // dd(Auth::user()->isAdmin());
            if (Auth::user()->isGold() || Auth::user()->isPremium()) {
                return redirect()->route('home.maindomain');
            } //allow admin to proceed with request
            else if (Auth::user()->isFree()) {
                return $next($request);
            }
        }

        abort(404); // for other user throw 404 error
        // return $next($request);
    }
}
