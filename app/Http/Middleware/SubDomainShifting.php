<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use DB;
use Illuminate\Support\Facades\Redirect;
use Session;

class SubDomainShifting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    private $SubDomain;
    private $user;

    public function handle($request, Closure $next)
    {
        // $this->user = Auth::user();
        // dd('hello');
        $this->getsubdomain(); //// open in production

        $data = DB::Connection('maindb')->select(DB::raw("select * from users where subdomain='" . $this->SubDomain . "'"));

        $config = array();

        if (!empty($data)) {

            $config['id'] = $data[0]->id;
            $config['host'] = 'localhost';
            $config['db_name'] = $data[0]->subdb;
            // $config['user'] = 'root'; //$data[0]->dbuser;
            // $config['password'] = ''; //$data[0]->dbpass;
            // Session::put('company_id', $data[0]->id);
            $this->setSession($data[0]->id);
            $userinfo = ['id' => $data[0]->id,
                'subdomain' => $data[0]->subdomain,
                'email' => $data[0]->email,
                'mobile_verified' => $data[0]->mobile_verified,
                'vcard' => 11];
            Session::put('userInfo', $userinfo);
            Config::set('database.connections.subdb.database', $config['db_name']);

        } else {
            //default values
            $config['id'] = "0";
            $config['host'] = "localhost";
            $config['db_name'] = Config::get('database.connections.maindb.database');
            $config['user'] = Config::get('database.connections.maindb.username');
            $config['password'] = Config::get('database.connections.maindb.password');
            // Session::put('company_id', 0);
            $this->setSession(0);

            //return response()->view('landing');

        }

        // dd($config);

        // Config::set('database.connections.subdb.host', $config['host']);
        // Config::set('database.connections.subdb.database', $config['db_name']);
        // Config::set('database.connections.subdb.username', $config['user']);
        // Config::set('database.connections.subdb.password', $config['password']);

        // Config::set('database.default', 'subdb');

        return $next($request);
    }

    public function getsubdomain()
    {
        //first if for check subdomain is present and not the main site
        if (!empty($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME'] != env("APP_BASE_URL"))) {
            $data = explode('.', $_SERVER['SERVER_NAME']);

            if (!empty($data[0])) {
                $this->SubDomain = $data[0]; // The * of *.mydummyapp.com will be now stored in $this->SubDomain
            }
        } else {
            // dd('home');
            return Redirect::to('/');
        }

        //return $this->SubDomain;
    }

    public function setSession($company_id)
    {

        if (!Session::has('company_id')) {
            Session::put('company_id', $company_id);
        } else if (Session::get('company_id') != $company_id) {
            abort(401);

            // dd(Session::get('company_id'));
        }
    }

    public function CheckSubdomain($subDomain, $userDomain)
    {

    }
}
