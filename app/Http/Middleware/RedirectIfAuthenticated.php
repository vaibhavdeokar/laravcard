<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            // $subdomain = Auth::user()->subdomain;
            // $url = \Config::get('app.base_url');
            // return redirect()->to('http://' . $subdomain . '.' . $url . '/dashboard');
            return redirect('home');
        }

        return $next($request);
    }
}
