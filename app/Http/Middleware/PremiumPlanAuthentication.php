<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class PremiumPlanAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            // dd(Auth::user()->isAdmin());
            if (Auth::user()->isFree() || Auth::user()->isGold()) {
                return redirect()->route('home.maindomain');
            } //allow admin to proceed with request
            else if (Auth::user()->isPremium()) {
                return $next($request);
            }
        }

        abort(404); // for other user throw 404 error
        // return $next($request);
    }
}
