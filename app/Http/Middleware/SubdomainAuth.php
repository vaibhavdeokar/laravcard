<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class SubdomainAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if ($user->mobile_verified == 0) {
            return redirect()->route('home.maindomain')->with('success', 'Please Verified your mobile first');
        }
        return $next($request);
    }
}
