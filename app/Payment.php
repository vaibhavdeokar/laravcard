<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_payment';

    protected $fillable = [
        'sname', 'svalue',
    ];
}
