<?php

namespace App\Classes;

use App\Classes\cPanel;

class CreateData
{
    public $cPanel;

    public function dataCreate($cPanel, $dbname, $subdomain)
    {
        $cPanel = new cPanel("easycukv", "Easy#321#", "119.18.58.80");

        //return $cPanel;

        $parameter = ['name' => $dbname];
        $result = $cPanel->execute('uapi', 'Mysql', 'create_database', $parameter);
        if (!$result->status == 1) {
            echo "Cannot create database : {$result->errors[0]}";

        } else {
            echo "Database creation successful.";
        }

        $dbparameter = ['privileges' => 'ALL PRIVILEGES',
            'database' => $dbname,
            'user' => 'easycukv_subuser'];
        $result = $cPanel->execute('uapi', 'Mysql', 'set_privileges_on_database', $dbparameter);
        if (!$result->status == 1) {
            echo "Cannot attach User to  database : {$result->errors[0]}";

        } else {
            echo "User add to Database successful.";
        }

        $parameters = [
            'domain' => $subdomain,
            'rootdomain' => "easycards.in",
            'dir' => "/public_html/public",
            'disallowdot' => 1,
        ];
        $results = $cPanel->execute('api2', "SubDomain", "addsubdomain", $parameters);

        if (isset($result->cpanelresult->error)) {
            echo "Cannot add sub domain : {$result->cpanelresult->error} ";

        } else {
            echo "Sub domain added successfully";
        }

    }
}
