<?php

namespace App\Home;

class MSG91
{
    public function __construct()
    {

    }

    public function sendotp($otp, $mobileno)
    {
        $isError = 0;
        $errorMessage = true;

        $curl = curl_init();

        $authkey = ''; //'330192Awcigt58p5f041815P1';  //open in production
        $tempid = '5efc3e43d6fc057a44370cda';
        $url = 'https://api.msg91.com/api/v5/otp?authkey=' . $authkey . '&template_id=' . $tempid . '&extra_param={"COMPANY_NAME":"EasyCards.in"}&mobile=91' . $mobileno . '&invisible=1&otp=' . $otp;
        // $url = "https://api.msg91.com/api/v5/otp?authkey=$authkey%20Key&template_id=$tempid%20ID&extra_param=%7B%22COMPANY_NAME%22%3A%22Easycards.in%22%2C%20%22OTP%22%3A%22$otp%22%2C%20%22Param3%22%3A%20%22Value3%22%7D&mobile=$mobileno%20with%20Country%20Code&invisible=1&otp=OTP%20to%20send%20and%20verify.%20If%20not%20sent%2C%20OTP%20will%20be%20generated.&userip=IPV4%20User%20IP&email=Email%20ID&otp_length=&otp_expiry=";
        //return array('error' => 1, 'message' => $url);
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_HTTPHEADER => array(
                "content-type: application/json",
            ),
        ));

        //Ignore SSL certificate verification
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($curl);

        //Print error if any
        if (curl_errno($curl)) {
            $isError = true;
            $errorMessage = curl_error($curl);
        }
        curl_close($curl);

        if ($isError) {
            return array('error' => 1, 'message' => $errorMessage);
        } else {
            return array('error' => 0);
        }
    }
    public function sendSMS($OTP, $mobileNumber)
    {
        $isError = 0;
        $errorMessage = true;

        //Your message to send, Adding URL encoding.
        $message = urlencode("Welcome to " . env('APP_BASE_URL') . " , Your OPT is : $OTP");

        //Preparing post parameters
        $postData = array(
            'authkey' => '330192Awcigt58p5f041815P1',
            'template_id' => '5efc3e43d6fc057a44370cda',
            'mobile' => $mobileNumber,
            'extra_param' => '{"COMPANY_NAME":"Easycards.com", "OTP":' . $OTP . '}',
        );

        $url = "https://api.msg91.com/api/v5/otp";

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData,
        ));

        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        //get response
        $output = curl_exec($ch);

        //Print error if any
        if (curl_errno($ch)) {
            $isError = true;
            $errorMessage = curl_error($ch);
        }
        curl_close($ch);

        if ($isError) {
            return array('error' => 1, 'message' => $errorMessage);
        } else {
            return array('error' => 0);
        }
    }
}
