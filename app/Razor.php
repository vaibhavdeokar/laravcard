<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Razor extends Model
{
    protected $connection = 'maindb';

    protected $table = 'razor_payment';

    protected $fillable = [
        'userid', 'payid', 'subid',
    ];
}
