<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_experience';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'sduration', 'sdesp',
    ];
}
