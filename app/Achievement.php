<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_achievement';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'svalue',
    ];
}
