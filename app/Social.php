<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_social';

    protected $fillable = [
        'sname', 'svalue',
    ];
}
