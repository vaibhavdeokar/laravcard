<?php

namespace App;

use DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasSubscriptions;

    protected $connection = 'maindb';

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'subdomain', 'mobile_verified', 'mobile_otp', 'subdb', 'referrer_id', 'referral_token', 'referral_code',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function referrer()
    {
        return $this->belongsTo(User::class, 'referrer_id', 'id');
    }

    public function referrals()
    {
        return $this->hasMany(User::class, 'referrer_id', 'id');
    }

    protected $appends = ['referral_link'];

    public function getReferralLinkAttribute()
    {
        return $this->referral_link = route('register', ['ref' => $this->username]);
    }

    public function isFree()
    {
        $role = DB::table('plan_subscriptions')->where('user_id', $this->id)->first();
        return $role->plan_id === 1;
    }

    public function isPremium()
    {
        $role = DB::table('plan_subscriptions')->where('user_id', $this->id)->first();
        return $role->plan_id === 2;
    }

    public function isGold()
    {
        $role = DB::table('plan_subscriptions')->where('user_id', $this->id)->first();
        return $role->plan_id === 3;
    }
}
