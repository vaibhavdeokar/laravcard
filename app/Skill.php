<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_skills';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'svalue',
    ];
}
