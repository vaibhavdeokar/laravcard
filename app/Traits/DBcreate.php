<?php

namespace App\Traits;

use App\Classes\cPanel;
use App\ColorSchema;

trait DBcreate
{
    // protected $cPanel;
    public function CreateDB($dbname, $subdomain)
    {
        $cPanel = new cPanel("easycukv", "Easy#321#", "119.18.58.80");

        $parameter = ['name' => $dbname];
        $result = $cPanel->execute('uapi', 'Mysql', 'create_database', $parameter);
        if (!$result->status == 1) {
            echo "Cannot create database : {$result->errors[0]}";

        } else {
            echo "Database creation successful.";
            $this->AssignUser($dbname, 'easycukv_subuser', $cPanel);
            $this->CreateSubDomain($subdomain, $cPanel);
            \Config::set('database.connections.subdb.database', $dbname);
        }
    }

    public function AssignUser($dbname, $dbuser, $cPanel)
    {
        $dbparameter = ['privileges' => 'ALL PRIVILEGES',
            'database' => $dbname,
            'user' => $dbuser];
        $result = $cPanel->execute('uapi', 'Mysql', 'set_privileges_on_database', $dbparameter);
        if (!$result->status == 1) {
            echo "Cannot attach User to  database : {$result->errors[0]}";
        } else {
            echo "User add to Database successful.";
        }
    }

    public function CreateSubDomain($subdomain, $cPanel)
    {
        $parameters = [
            'domain' => $subdomain,
            'rootdomain' => 'easycards.in',
            'dir' => "/public_html/public",
            'disallowdot' => 1,
        ];
        $results = $cPanel->execute('api2', "SubDomain", "addsubdomain", $parameters);

        if (isset($result->cpanelresult->error)) {
            echo "Cannot add sub domain : {$result->cpanelresult->error} ";

        } else {
            echo "Sub domain added successfully";
        }
    }

    public function TwoColumnTable($tblname)
    {

        if (!Schema::connection('subdb')->hasTable($tblname)) {
            Schema::connection('subdb')->create($tblname, function (Blueprint $table) {
                $table->bigInteger('id', true)->unsigned();
                $table->string('sname', 100)->nullable();
                $table->text('svalue')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::connection('subdb')->drop($tblname);
            $this->TwoColumnTable($tblname);
        }

    }
    public function ThreeColumnTable($tblname)
    {
        if (!Schema::connection('subdb')->hasTable($tblname)) {
            Schema::connection('subdb')->create($tblname, function (Blueprint $table) {
                $table->bigInteger('id', true)->unsigned();
                $table->string('sname', 100)->nullable();
                $table->string('sduration', 255)->nullable();
                $table->text('sdesp')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::connection('subdb')->drop($tblname);
            $this->ThreeColumnTable($tblname);
        }
    }

    public function createPersonalTable()
    {
        if (!Schema::connection('subdb')->hasTable('tbl_info')) {
            Schema::connection('subdb')->create('tbl_info', function (Blueprint $table) {
                $table->bigInteger('id', true)->unsigned();
                $table->string('first_name', 100)->nullable();
                $table->string('last_name', 100)->nullable();
                $table->string('profession', 100)->nullable();
                $table->string('mobile', 50)->nullable();
                $table->string('phone', 50)->nullable();
                $table->string('email', 100)->nullable();
                $table->string('website', 100)->nullable();
                $table->text('address')->nullable();
                $table->text('about_me')->nullable();
                $table->string('skype_id', 100)->nullable();
                $table->string('fb_msg', 100)->nullable();
                $table->string('youtube_about', 100)->nullable();
                $table->string('youtube_company', 100)->nullable();
                $table->string('cname', 100)->nullable();
                $table->string('ctagline', 100)->nullable();
                $table->text('cabout', 100)->nullable();
                $table->string('cmap', 100)->nullable();
                $table->string('profile_logo')->nullable();
                $table->string('company_logo')->nullable();
                $table->timestamps();
            });
        } else {
            Schema::connection('subdb')->drop('tbl_info');
            $this->createTable();
        }
    }

    public function FillPersonalInfo($name, $email)
    {
        $info = new Userinfo();
        $info->setConnection('subdb');
        $info->first_name = $name;
        $info->email = $email;
        $info->mobile = Session::get('mobileno');
        $info->save();
    }

    public function FillColorTbl()
    {
        $info = new ColorSchema();
        $info->setConnection('subdb');
        $info->sname = 'color1';
        $info->svalue = '#000000';
        $info->save();
        $info->sname = 'color2';
        $info->svalue = '#000000';
        $info->save();
    }
}
