<?php

namespace App\Traits;

use App\SubUser;
use Auth;
use Illuminate\Http\Request;
use Redirect;

trait LoginTrait
{

    public function loginOther(Request $request)
    {

        $this->validateLoginOther($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        // if ($this->guard()->validate($this->credentials($request))) {
        if (Auth::guard('subsite')->validate($this->credentials($request))) {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                // return redirect()->intended('subdomain.dashboard');
                $subdomain = $request->subdomain;
                $url = \Config::get('app.base_url');
                $mainUrl = 'http://' . $subdomain . '.' . $url . '/dashboard';
                return $mainUrl;
                // return redirect()->route('subdomain.dashboard');
            } else {
                $this->incrementLoginAttempts($request);
                return response()->json([
                    'error' => 'This account is not activated.',
                ], 401);
            }
        } else {
            // dd('ok3');
            $this->incrementLoginAttempts($request);
            return response()->json([
                'error' => 'Credentials do not match our database.',
            ], 401);
        }
    }

    protected function validateLoginOther(Request $request)
    {
        $request->validate([
            // $this->username() => 'required|string',
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function authenticatedOther(Request $request, $user)
    {
        $subdomain = $user->subdomain;
        $url = \Config::get('app.base_url');
        return redirect()->to('http://' . $subdomain . '.' . $url . '/dashboard');
        return Redirect::to($url);
        // return redirect('/home');
    }

    public function autoLogin(Request $request)
    {
        // dd($request);
        $user = SubUser::where('email', $request->email)->first();
        Auth::login($user);
        return redirect('/home');
    }
}
