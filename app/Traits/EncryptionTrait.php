<?php

namespace App\Traits;

trait EncryptionTrait
{
    public function Encrypt($text)
    {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $encrypted = $encrypter->encrypt($text);
        return $encrypted;
    }

    public function Decrypt($value)
    {
        $encrypter = app('Illuminate\Contracts\Encryption\Encrypter');
        $decryptedDynamic = $encrypter->decrypt($value);
        return $decryptedDynamic;

        //other method
        // $decryptedHardCoded = Encrypter::decrypt($encryptedValue);

    }
}
