<?php

namespace App\Traits;

use Auth;

trait GetSubDomain
{
    public function getdb()
    {
        $user = Auth::user();
        \Config::set('database.connections.subdb.database', $user->subdb);
        //\Config::set('database.connections.subdb.username', 'root');
        //\Config::set('database.connections.subdb.password', '');
    }
}
