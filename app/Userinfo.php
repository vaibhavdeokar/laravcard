<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userinfo extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_info';

    // protected $guarded = [];
    protected $fillable = [
        // 'cname', 'ckey', 'cvalue',
        'first_name', 'last_name', 'profession', 'mobile', 'phone', 'email', 'website', 'address', 'about_me', 'skype_id', 'youtube_about', 'youtube_company', 'fb_msg', 'cname', 'ctagline', 'cabout', 'cmap', 'profile_logo', 'company_logo',
    ];
}
