<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_referral';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'svalue',
    ];
}
