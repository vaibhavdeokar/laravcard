<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_product';

    protected $fillable = [
        'sname', 'svalue',
    ];
}
