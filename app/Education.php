<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $connection = 'subdb';

    protected $table = 'tbl_education';

    // protected $guarded = [];
    protected $fillable = [
        'sname', 'sduration', 'sdesp',
    ];
}
