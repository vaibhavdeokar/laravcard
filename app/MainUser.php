<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class MainUser extends Authenticatable
{
    use Notifiable;

    protected $connection = 'maindb';

    protected $guard = 'mainsite';

    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'subdomain', 'mobile_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
